-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 13, 2018 at 10:54 AM
-- Server version: 5.5.60-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.25

--SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vatas`
--

-- --------------------------------------------------------

--
-- Table structure for table `annotations`
--

CREATE TABLE IF NOT EXISTS "annotations" (
  "id" serial NOT NULL,
  "item_id" bigint NOT NULL,
  "annotator" varchar(56) DEFAULT NULL,
  "location" varchar(16) NOT NULL,
  "location_code" varchar(56) NOT NULL,
  "location_text" text DEFAULT '',
  "threat_level" decimal(3,2) NOT NULL,
  "threat_explanation" text NOT NULL,
  "lean_likeliness" decimal(3,2) NOT NULL,
  "lean_explanation" text DEFAULT '',
  "comments" text NOT NULL,
  "bounding_boxes" text NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "first_impression" text,
  "communication_code" text,
  PRIMARY KEY ("id")
);

-- --------------------------------------------------------

--
-- Table structure for table `bbox_annotations`
--

CREATE TABLE IF NOT EXISTS "bbox_annotations" (
  "item_id" bigint NOT NULL,
  "id" serial NOT NULL,
  "annotator" varchar(56) DEFAULT NULL,
  "bounding_boxes" text NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY ("id")
);

-- --------------------------------------------------------

--
-- Table structure for table `code_annotations`
--

CREATE TABLE IF NOT EXISTS "code_annotations" (
  "item_id" bigint NOT NULL,
  "id" serial NOT NULL,
  "annotator" varchar(56) DEFAULT NULL,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "first_impression" text,
  "communication_code" text,
  PRIMARY KEY ("id")
);

-- --------------------------------------------------------

--
-- Table structure for table `collapsed_annotations`
--

CREATE TABLE IF NOT EXISTS "collapsed_annotations" (
  "item_id" bigint NOT NULL,
  "id" serial NOT NULL,
  "annotator" varchar(56) DEFAULT NULL,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "collapsed_code" text,
  PRIMARY KEY ("id")
);

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE IF NOT EXISTS "data" (
  "user_id" bigint DEFAULT NULL,
  "screen_name" varchar(15) DEFAULT NULL,
  "item_id" bigint DEFAULT NULL,
  "created_at" varchar(30) DEFAULT NULL,
  "retweet_count" int DEFAULT NULL,
  "text" varchar(148) DEFAULT NULL,
  "source" varchar(84) DEFAULT NULL,
  "url" varchar(46) DEFAULT NULL,
  "media_url" varchar(46) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `data_users`
--

CREATE TABLE IF NOT EXISTS "data_users" (
  "user_id" bigint NOT NULL DEFAULT '0',
  "available" boolean NOT NULL DEFAULT '1',
  "position" float NOT NULL DEFAULT '0',
  PRIMARY KEY ("user_id")
);

-- --------------------------------------------------------

--
-- Table structure for table `reconcile_annotations`
--

CREATE TABLE IF NOT EXISTS "reconcile_annotations" (
  "item_id" bigint NOT NULL,
  "id" serial NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "reconciled_task" varchar(128) NOT NULL,
  "annotator" varchar(56) DEFAULT NULL,
  "annotation1_id" int NOT NULL,
  "annotation2_id" int NOT NULL,
  "concept" varchar(128) NOT NULL,
  "decision" smallint NOT NULL,
  "bounding_boxes" text,
  "conflict_type" smallint NOT NULL DEFAULT '0',
  PRIMARY KEY ("id")
);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
