<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');
 
/***************************************************************************

    UTILITY FUNCTIONS FOR DISPLAYING INFORMATION TO USER

***************************************************************************/


/* TWITTER SPECIFIC FUNCTIONS */

function user_link($screen_name) {
  return '<a href="https://twitter.com/'.$screen_name.'/" target="_blank">@'.$screen_name.'</a>';
}

function hashtag_link($hashtag) {
  return '<a href="https://twitter.com/hashtag/'.$hashtag.'?src=hash" target="_blank">#'.$hashtag.'</a>';
}

function make_link($url) {
  return '<a href="'.$url.'" target="_blank">'.$url.'</a>';
}

function make_hashtag_links($text){
  return preg_replace_callback('/#([a-zA-Z0-9_-]+)/i', function($m) { return hashtag_link($m[1]); }, $text);
}

function make_screen_name_links($text){
  return preg_replace_callback('/@([a-zA-Z0-9_-]+)/i', function($m) { return user_link($m[1]); }, $text);
}

function make_links_clickable($text){
  return preg_replace_callback('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', function($m) { return make_link($m[1]); }, $text);
}


function display_tweet_div($tweet_url, $screen_name, $tweet_text, $posted_at, $image_url=Null, $bounding_box_annotator=False, $show_annotator=True) { ?>
  <div style="padding-bottom:10px; margin-right:10px; display:inline-block;">
    <h2 style="margin-top: 0; text-align:center; font-size:26px;"><a href="<?php echo $tweet_url; ?>" target="_blank">Tweet</a></h2>
     
    <div class="tweet" style="max-width:450px;">
      <span class="tweet_poster"><?php echo user_link($screen_name); ?></span>
      <?php if($tweet_text): ?>
        <span class="tweet_text"><?php echo make_hashtag_links(make_screen_name_links(make_links_clickable(htmlspecialchars($tweet_text)))); ?></span>
      <?php else: ?>
        <em>This tweet has no text.</em>
      <?php endif; // tweet has text ?>
      <span class="tweet_created"><?php echo $posted_at; ?></span>
    </div>
      
    <?php if($image_url) { ?>
      <div id="tweet_image"><img src="<?php echo $image_url; ?>" style="display:<?php echo $show_annotator ? 'none' : 'block'; ?>; max-width:450px;" />
      (<a href="<?php echo $image_url; ?>" target="_blank">View image in full size</a>)
      </div>
    <?php } ?>

    <?php if($bounding_box_annotator) { echo '<div id="bbox_annotator" '.($show_annotator ? '': 'style="display:none"').'></div>'; } ?>
  </div>   
<?php }


/* GENERAL DISPLAYING */

function instruction_box($i, $concept_name, $include, $title='Annotation Guidelines', $link_text='?', $append_text='') {
  // modal box used for displaying instructions
  ?>
  [<a class="modalLink" id="modalLink_<?php echo $i; ?>"><?php echo $link_text; ?></a>]<?php echo $append_text; ?>
  <div id="modal_<?php echo $i; ?>" class="modal">
    <div class="modal-content">
      <span class="closeModal" id="close_<?php echo $i; ?>">&times;</span>
      <h1><?php if($concept_name) echo $concept_name." &ndash; "; echo $title; ?></h1>
      <table class="table">
        <?php include(INSTRUCTIONS_PATH.$include); ?>
      </table>
    </div>
  </div>
  <script>
  // Get the modal
  var modal_<?php echo $i; ?> = document.getElementById('modal_<?php echo $i; ?>');

  // Get the button that opens the modal
  var btn_<?php echo $i; ?> = document.getElementById("modalLink_<?php echo $i; ?>");

  // Get the <span> element that closes the modal
  var span_<?php echo $i; ?> = document.getElementById("close_<?php echo $i; ?>");

  // When the user clicks on the button, open the modal
  btn_<?php echo $i; ?>.onclick = function() {
      modal_<?php echo $i; ?>.style.display = "block";
  }

  // When the user clicks on <span> (x), close the modal
  span_<?php echo $i; ?>.onclick = function() {
      modal_<?php echo $i; ?>.style.display = "none";
  }
  </script>
  <?php
}

function make_bbox_instructions($concepts, $instruction_files, $offset=1) {
  ?>
  <p>Please draw bounding boxes directly on the shown image to mark all of the concepts below that are displayed in bold.
  (Make sure you are familiar with the <?php instruction_box(0, "", "general.php", "General Annotation Guidelines", "general guidelines", ".)"); ?></p>
    
  <ul>
  <?php
  for($i = 0; $i<sizeof($concepts); $i++) {
    echo '<li><strong>'.$concepts[$i].'</strong> ';
    if(array_key_exists($concepts[$i], $instruction_files)) {
      instruction_box($i+$offset, $concepts[$i], $instruction_files[$concepts[$i]]);
    }
    echo '</li>';
  } ?>
  </ul>
  <p>Click the question marks to show additional information for the corresponding concepts.</p>
  <?php
}

?>
