<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

/**
 * STATUS MESSAGES
 * This file defines classes and functions used for displaying status messages to the user.
 */

abstract class Messages {
    // save messages as plain text arrays
    // The text shouldn't include any interface dependent tags
    // since Messages should be interface independent.
    static private $errors = array();
    static private $warnings = array();
    static private $status = array();

    // The custom error handler
    static public function error_handler($errno, $errstr, $errfile, $errline) {
        // Save the error or warning depending on the errortype
        switch ($errno) {
            case E_USER_ERROR:
                Messages::add_error( "[$errno] $errstr (in $errfile on line $errline)" );
                break;

            case E_USER_WARNING:
                if( DEBUG_MODE) Messages::add_warning( "[$errno] $errstr (in $errfile on line $errline)");
                else Messages::add_warning( $errstr );
                break;

            case E_USER_NOTICE:
                if( DEBUG_MODE) Messages::add_warning( "[$errno] $errstr (in $errfile on line $errline)");
                else Messages::add_warning( $errstr );
                break;

            default:
                Messages::add_error( "Unknown errortype: [$errno] $errstr (in $errfile on line $errline)" );
                break;
            }

        // Don't execute the original PHP error handler.
        return true;
    }

    // Those methods are rather trivial:
    // There are methods to add new messages and methods to get
    // all existings ones.
    // Removing any messages doesn't seem to be necessary.
    static public function add_error( $error_text ) { self::$errors[] = (string)$error_text; }
    static public function add_warning( $warning_text ) { self::$warnings[] = (string)$warning_text; }
    static public function add_status( $status_text ) { self::$status[] = (string)$status_text; }

    static public function get_errors() { return self::$errors; }
    static public function get_warnings() { return self::$warnings; }
    static public function get_status() { return self::$status; }
}

abstract class Statusbox {

    static public function content() {
        self::add_error_messages();
        self::add_warnings();
        self::add_status();
    }

    // Output all messages of Messages::$errors
    static public function add_error_messages() {
        $errors = Messages::get_errors();

        if( count($errors)>0 ) {
            ?><div class="errors"><ul><?php

            foreach( $errors as $error ) {
                echo '<li>'.$error.'</li>';
            }

            ?></ul></div><?php
        }
    }

    // Output all messages of Messages::$warnings
    static public function add_warnings() {
        $warnings = Messages::get_warnings();

        if( count($warnings)>0 ) {
            ?><div class="warnings"><ul><?php

            foreach( $warnings as $warning ) {
                echo '<li>'.$warning.'</li>';
            }

            ?></ul></div><?php
        }
    }

    // Output all messages of Messages::$status
    static public function add_status() {
       $status = Messages::get_status();

        if( count($status)>0 ) {
            ?><div class="messages"><ul><?php

            foreach( $status as $s ) {
                echo '<li>'.$s.'</li>';
            }

            ?></ul></div><?php
        }
    }
}

// Set the custom error handler
set_error_handler("Messages::error_handler");

// Define the status handler
function status_message( $message ) {
    if( !is_string( $message ) ) {
        trigger_error( 'Bad input!', E_USER_WARNING );
        return false;
    }

    Messages::add_status($message);
}
?>
