<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

/***************************************************************************

    GENERAL DATABASE FUNCTIONS
    
    This file contains functions to interact with a PostgreSQL database.

***************************************************************************/


// Connect to the database
$db_connection = pg_connect("host=".DB_HOST." user=".DB_USER." password=".DB_PASSWORD." dbname=".DB_DB." options='--client_encoding=UTF8'");

// Perform a database query (connection required)
function db_query( $query ) {
  global $db_connection;
  $result = pg_query($db_connection, $query);

  if( !$result ) {
      trigger_error( 'Database error: '.pg_last_error().' (Whole query: "'.$query.'")', E_USER_WARNING );
  }
  return $result;
}

function db_fetch_assoc($stuff) {
  return pg_fetch_assoc($stuff);
}

// Make $value save for database query
function db_escape( $value, $add_strips=false ) {
  global $db_connection;

  if( is_numeric($value) ) return intval($value);

  $value = @trim($value);
	  
  if(get_magic_quotes_gpc()) {
	  $value = stripslashes($value);
  }

  $value = pg_escape_string($db_connection, $value);

  return ($add_strips ? "'".$value."'" : $value);    
}

/* More safety functions (for processing user input before writing to DB) */
function sanitize($input) {
  $pattern = "/[^ \na-zA-Z0-9_öäüÖÄÜß?.!()\"'\/-]/";
  $filtered = db_escape(preg_replace($pattern, "", $input),true);
  return $filtered;
}
function bb_sanitize($input) {
  // The bounding box information is stored in JSON format, so we can't be quite as restrictive regarding the allowed characters
  $pattern = "/[^ a-zA-Z0-9,\/\]\[{}()\":-]/";
  $filtered = db_escape(preg_replace($pattern, "", htmlspecialchars_decode(trim(preg_replace('/\s+/', ' ', $input)))),true);
  return $filtered;
}


// Convert (key1=>value1,key2=>value2,...) to
// "key1 = value1 $connector key2 = value2 $connector ..."
// Can be used for simple where or update queries for example.
// CAVE: Values are assumed to be escaped already!
function db_array_to_text($value_array, $connector='AND') {
  // Check some feasibility
  if( !is_array($value_array) || count($value_array)<1 ) {
      trigger_error( 'Bad input!', E_USER_WARNING );
      return false;
  }

  $texts = array();
  foreach( $value_array as $column=>$value ) {
    if(is_array($value)) {
      $texts[] = db_membership_string($column, $value);
    } else {
      $texts[] = $column." = ".$value;
    }
  }

  return implode(" ".$connector." ", $texts);
}

function parentesize($string) {
  return '('.$string.')';
}

function db_membership_string($column_name, $values) {
  return $column_name." IN (VALUES ".implode(', ', array_map("parentesize", array_map("sanitize",$values))).")";
}
  
function db_get_row_where_values($table, $value_array) {
  $element = db_query("SELECT * FROM ".$table." WHERE ".db_array_to_text($value_array).";");

  if( $element && $element=db_fetch_assoc($element) ) {
      return $element;
  }
  else {
    return False;
  }
}

function db_get_rows_where_values($table, $value_array) {
  $query = "SELECT * FROM ".$table." WHERE ".db_array_to_text($value_array).";";

  return db_elements_from_query($query);
}

function db_get_all_rows($table) {
  return db_elements_from_query("SELECT * FROM ".$table.";");
}

function db_get_row_by_id($id, $table) {
  return db_get_row_where_values($table, array("id"=>intval($id)));
}


function db_element_from_query($query) {
  $element = db_query($query);

  if( $element && $element=db_fetch_assoc($element) ) {
      return $element;
  }
  else {
    return False;
  }
}

function db_elements_from_query($query) {
  $result = db_query($query);

  if(!$result) {
    return array();
  }

  $responses = array();
  while($element=db_fetch_assoc($result) ) {
      array_push($responses,$element);
  }
  return $responses;
}


// Update an existing entry with the values $entry (column_name=>value)
// in table $table.
function db_update_entry( $table, $entry, $where_array ) {
  // Check some feasibility stuff
  if( !$table || !is_array($entry) || count($entry)<1 ) {
      trigger_error( 'Bad input!', E_USER_WARNING );
      return false;
  }

	$sql = "UPDATE " . $table . " SET ".db_array_to_text($entry, ", ")." WHERE ".db_array_to_text($where_array).";";
			
  // perform the DB query  
  if(db_query($sql)) {
    status_message("The database has been updated.");
    return True;
  } else {
    trigger_error("Database could not be updated!", E_USER_ERROR);
    return False;
  }
}


function db_delete_entry($table, $id) {
  $sql = "DELETE FROM ".$table." WHERE id=".intval($id).";";

  // write to DB
  $result = db_query($sql);
  if($result) {
    status_message("Successfully deleted element from the database.");
    return True;
  } else {
    trigger_error("The element could not be deleted!", E_USER_ERROR);
    return False;
  }
}


function db_store_entry($table, $value_array) {
  $typetext = join(', ', array_keys($value_array));
  $entrytext = join(', ', array_values($value_array));
  $sql = "INSERT INTO ".$table." (" . $typetext . ") VALUES (" . $entrytext . ");";

  // write to DB
  $result = db_query($sql);
  if($result) {
    status_message("Your response was written to the database.");
    return True;
  } else {
    trigger_error("Your previous response could not be saved!", E_USER_ERROR);
    return False;
  }
}

?>
