<?php

// This contant is defined for security reasons
// (included php files are only interpreted if it is set)
if ( !defined('MAIN_FILE_INCLUDED') )
    define('MAIN_FILE_INCLUDED', true);

// Absolute path to the main directory.
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	

// load system config
require ABSPATH.'config/system_config.php';


// load tasks and extend PAGES array accordingly
require_once ABSPATH.'config/tasks.php';
foreach($TASKS as $name=>$task) {
  $PAGES[$name] = array('name'=>$name, 'title'=>$task['title']);
}


session_start();

/**
 * LOADING PAGE INFORMATION
 *
 * The requested page is load only if the user is logged in
 * OR if user information was sent in the POST variable.
 * (The later version is implemented to make sure that responses are
 * saved even if the user session was terminated during the annotation process.)
 */
if (((isset($_SESSION['annotator']) and isset($_SESSION['system']) and $_SESSION['system']=='images') or (isset($_POST['annotator']) and isset($_POST['annotator_pass']) and $_POST['annotator_pass']==md5($USERS[$_POST['annotator']]['password'])))
    and isset($_GET['p']) and array_key_exists($_GET['p'], $PAGES)) {
  $page = $PAGES[$_GET['p']];
  $annotator = isset($_SESSION['annotator']) ? $_SESSION['annotator'] : $_POST['annotator'];
} else {
  $page = $PAGES['login'];
}

function admin_rights() {
  global $annotator, $USERS;
  
  if($USERS[$annotator]['is_admin']=='t' or $USERS[$annotator]['is_admin']===True) return True;
  else return False;
}

function i_am_expert() {
  global $annotator, $USERS;
  
  if($USERS[$annotator]['is_expert']=='t' or $USERS[$annotator]['is_expert']===True) return True;
  else return False;
}

// custom error reporting and status messages
require FUNCTION_PATH.'Messages.class.php';

// database functionality
require FUNCTION_PATH.'database.'.DB_TYPE.'.php';

// load task specific functions (if we are on a task page)
if(array_key_exists($page['name'], $TASKS)) {
  $TASK_NAME = $page['name'];
  include ABSPATH.'tasks/'.$TASKS[$TASK_NAME]['header_name'].'.php';
}

header('Content-Type:text/html; charset=UTF-8');
?><!DOCTYPE html>
<html>
<head>
	<title><?php if (isset($page['title'])) echo $page['title']; ?></title>

	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css" >
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
	<!-- Functionality for bounding box annotations -->
    <script src="bbox_annotator.js"></script>
</head>
<body>
<div id="container">
    
<?php Statusbox::content(); // displaying status and error messages ?>

<?php if ($page['name']!='login'): ?>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">

	<div class="navbar-header">
	  <a class="navbar-brand" href="#"><?php if(isset($page['title'])) echo $page['title']; ?></a>      
	</div>
	
	<div id="navbar" class="collapse navbar-collapse">
	  
	  <ul class="nav navbar-nav pull-right">

    <?php 
    // admin pages
    if(admin_rights()) { ?>
      <li><a href="index.php?p=export">Export</a></li>
    <?php } ?>
    <li><a href="index.php?p=annotations">Annotation overview</a></li>
	    
	  <li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Select task <span class="caret"></span></a>
		<ul class="dropdown-menu">
		<?php foreach($TASKS as $name=>$task) {
		  // Special tasks for reconciling are displayed on second level
		  if(in_array($name, $RECONCILE_TASKS)) {
		    continue;
		  }
		  
		  if(in_array($annotator,$task['users']) or in_array($annotator,$task['experts']) or admin_rights()) {
		    ?>
		    <li><a href="index.php?p=<?php echo $name; ?>"><?php echo $task['title']; ?></a></li>
		    <?php if(isset($task['field_reconcile_tasks']) and admin_rights()) {
		      foreach($task['field_reconcile_tasks'] as $field=>$reconcile_task) {
		        echo '<li><a href="index.php?p='.$reconcile_task.'&amp;task='.$name.'">-- '.$TASKS[$reconcile_task]['title'].'</a></li>';
		      }
		    } ?>
		<?php }
		} ?>
		</ul>
	      </li>
	      <li><a href='logout.php'>Logout (<b><?php echo $annotator; ?>)</b></a></li>
	  </ul>
	</div><!--/.nav-collapse -->
    
    </div>
  </nav>
<?php endif; // pagename!='login' ?>

<?php 
// Load the body of the page
if(array_key_exists($page['name'], $TASKS)) {
  // If we are on a task page, load this page according to settings
  include ABSPATH.'pages/'.$TASKS[$page['name']]['page'].'.php';
} else {
  include ABSPATH.'pages/'.$page['name'].'.php';
}
?>

</div><!-- container -->
</body>
</html>
