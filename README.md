# VATAS: An Open-Source Web Platform for Visual and Textual Analysis System of Social media


VATAS is a free open-source system for annotating social media data
with selected and trained annotators.

The system is highly customizable, includes bounding box image annotation functionality
and is designed specifically for collaborations
between social work researchers and computer scientists,
where rigorous analysis is performed during annotation
and the annotated data might be used for further qualitative analysis and
building computational models.


## Citation

If any of this code was helpful for your research, please consider citing it:

    @article{patton2019vatas,
      title={VATAS: An Open-Source Web Platform for Visual and Textual Analysis of Social Media},
      author={Patton, Desmond and Blandfort, Philipp and Frey, William R. and Gaskell, Michael and Schifanella, Rossano and Chang, Shih-Fu},
      journal={Journal of the Society for Social Work and Research},
      year={2019 (forthcoming)}
    }
    

## Disclaimer

This code is not meant to be used for any form of public annotation such as crowdsourcing.
In particular you should be aware that this system
- does not include any cookie or privacy policy notifications and
- is _not_ secure and therefore should either be used only with annotators you trust or be modified by a security expert.


## Setting up VATAS

1. Have a webserver installed which supports PHP and PostgreSQL/MySQL.
    - If you need to set up the webserver yourself, a common choice would be to use Apache. (See, for example, https://medium.com/@Riverside/how-to-install-apache-php-postgresql-lapp-on-ubuntu-16-04-adb00042c45d.)
    - For setting up PostgreSQL, the following link might be useful: https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart
2. Get the code. (E.g., in the terminal run `git clone https://gitlab.com/blandfort/VATAS/ <local_dir_name>`.)
3. Set up the database.
    1. Create a new database if necessary: e.g. in terminal open PostgreSQL by `psql` and execute
  
        ```
        create database vatas encoding='utf-8';
        ```

    2. Create a database user.
    2. Create all tables. You can import the database dump from data/vatas_structure.sql for this step (tested for PostgreSQL)
    3. Grant rights to database user if necessary.
  
        ```
        grant select on all tables in schema public to [USERNAME];
        grant update on table data_users to [USERNAME];
        grant update,insert,delete on table annotations to [USERNAME];
        grant update,insert,delete on table bbox_annotations to [USERNAME];
        grant update,insert,delete on table code_annotations to [USERNAME];
        grant update,insert,delete on table reconcile_annotations to [USERNAME];
        grant update,insert,delete on table collapsed_annotations to [USERNAME];
        grant usage,select on all sequences in schema public to [USERNAME];
        ```
    
4. Adjust the system configuration file (`config/system_config.php`). See below for additional details. 
5. Adjust the task settings and add data to the system.
    - For example, for loading the demo data (TODO link to these files) with PostgreSQL, open PostgreSQL and connect to the database in terminal by ```psql [DB_NAME]```, then execute

        ```
        COPY data FROM '/path/to/tweets.csv' DELIMITER ',' CSV HEADER;
        COPY data_users FROM '/path/to/tweet_users.csv' DELIMITER ',' CSV HEADER;
        ```

__Note__: VATAS has only been deployed and tested on Linux systems so far (Ubuntu, Debian and Linux Mint).


## Steps for adding a new task

1. Pick a name for the task (TASK_NAME) and a name for the associated PHP class (TASK_CLASS_NAME).
1. In `tasks/` create files `[TASK_CLASS_NAME].class.php` and `[TASK_NAME].php`:
    - `[TASK_CLASS_NAME].class.php` defines the task class that is used to load data for annotation, store, edit and delete annotations and finding out which fields belong to the task.
    For standard tasks, this class can inherit all these functions from the BaseTask class (defined in `tasks/generic/BaseClass.class.php`) and you only need to modify the constructor
    such that the task fields are specified. (See `tasks/generic/fields.php` for available fields.) For a simple example see `tasks/CodeTask.class.php`.
    - `[TASK_NAME].php` consists of 3 parts:
    First, the task class is instantiated, normally in the variable `$TASK`.
    Second, requests (storing, updating, deleting annotations) are processed and an item is retrieved for annotation.
    Finally, the layout of the annotation task is specified. How exactly this is done depends on the page template (file in `pages/`) you want to use for displaying.
    A rather simple example that uses the tweet task template (`pages/tweet_task.php`) is given by `tasks/code_task.php`.
    - Note that both files are included before any HTML content is created. In particular, this means that redirects can be implemented there and HTML contents should not directly be output.
2. Add database tables that are required (typically these are 3 tables, for the social media data, the social media users and the annotations respectively):
    - The _annotation table_ needs to contain the columns `id` (to identify individual annotations), `annotator` (to store the name of the annotator) and `created_at` (mostly for ordering).
    The remaining column names have to be in line with the field name specifications in the corresponding `[TASK_CLASS_NAME].class.php` file.
    - The _data table_ is assumed to include a column `item_id` and a column `user_id`. Text associated with the items should be included in a `text` column,
    associated multimedia such as pictures should be referred to in a `media_url` column. 
    Which other fields are required depends on the page template that is used for displaying the task.
    - The _user table_ stores information on availability and ordering of data item authors. It is assumed to contain at least the columns `user_id`, `available` and `position`.
    - Note: Tables can be shared across tasks. In particular, sharing tables often makes sense for the data and user tables.
    - You might need to enable your database user to access the new database by granting rights, similar to above. For example, in case of PostgreSQL in the terminal go to the database by ```psql [DB_NAME]``` and execute

        ```
        grant select,update,insert,delete on table [NEW_TABLE_NAME] to [USERNAME];
        grant usage,select on all sequences in schema public to [USERNAME];
        ```

3. Add the task settings to `config/tasks.php`.
4. If necessary add a page for displaying the task (in `pages/`). This file is meant to display the item for annotation to the user and define the form for collecting the user response.
  Individual form fields can generally created by calling `$TASK->get_fields()[FIELD_NAME]->display();`.
  If you need the page for displaying a single task only, this can mostly be done with HTML.
  For a more flexible template, you may have a look at `pages/tweet_task.php`.


## Notes on system configuration (config/system_config.php)

System pages:

- "name": unique name of the page (used for identifying the page); for each page there has to exist a file `[PAGENAME].php` in the `pages/` directory
- "title": title of the page that is shown to the user

Users:

- "name": Usernames need to be unique and should not be changed.
- "is_admin": If set to 't', the use has administrative rights and is able to see all tasks, view and edit all annotations and export annotations as CSV files.
- "password": Use the `md5` hash function on passwords to store them. Note that
    - user passwords of the default users are identical to their respective user names. These settings are meant to be used for illustration purposes only.
    - in linux the md5 hash of a password string can be computed in the terminal by calling "echo -n [PASSWORD] | md5sum".


## Code structure

- `config/`: System settings (including user accounts) and task configurations
- `tasks/`: Defining individual tasks, where `tasks/generic/` contains code that is useful for most tasks
  and `tasks/instructions/` is used for storing contents for bounding box annotation instructions.
- `pages/`: Code that is used for finally displaying contents to the user.
- `functions/`: General displaying and database functionality


## Other notes

- For social media data different from tweets, some displaying functions need to be adjusted. (`functions/displaying.php`)

## Acknowledgements

Functionality for bounding box annotation (bbox_annotator.js) is based on JavaScript code generated by the bbox-annotator of Kota Yamaguchi (https://github.com/kyamagu/bbox-annotator).

