<?php
// Terminate the session and redirect to the main page (where a login screen will be shown).
session_start();
session_destroy();
header('Location: index.php');
?>
