<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

// Include displaying functions
include_once(FUNCTION_PATH.'displaying.php');


// Randomly decide which annotation is shown on which side
$displaying_order = rand(1,2);


// function to display an existing boundig box annotation
function annotation_td($td_id, $annotation, $img_url, $image_width, $target_page_name) {
  ?>
	<button type="submit" name="vote<?php echo $td_id; ?>" class="image_vote">
	<div id="bbox_annotator<?php echo $td_id; ?>" class="bbox_annotator" style="display:inline-block">
	  <img id="tweet_image" src="<?php echo $img_url; ?>" style="width:<?php echo $image_width; ?>px; position:relative" />
        </div>
        </button>
        <br /><br />(<a href="index.php?p=<?php echo $target_page_name; ?>&amp;id=<?php echo $annotation['id']; ?>&amp;field=bounding_boxes" target="_blank">Go to annotation</a>)
  <?php
}


if($item): // we only show the form if there is an item for annotation ?>

<form method="POST" action="index.php?p=<?php echo $TASK_NAME; ?>&amp;task=<?php echo $TASK->reconciled_task->get_name(); ?>" style="margin-top:15px;">

  <h1 style="text-align:center; margin-top:80px;">Which annotation is better for '<?php echo $TASK->get_fields()['concept']->get_value(); ?>'?</h1>
  <div style="text-align:center;">(Detected issue: '<?php echo $TASK->reconciled_task->get_fields()['bounding_boxes']->conflict_meaning($TASK->get_fields()['conflict_type']->get_value()); ?>')</div>

    <!--<h2>People and Objects</h2>-->
    <div style="display:none;">
      <textarea id="annotation_data" name="annotation" rows="30" style="font-family:monospace;" readonly></textarea>
    </div>


  <table class="comparison_table">

  <tr>
	  <td>
          <?php annotation_td($displaying_order, $displaying_order==1 ? $TASK->get_first_annotation() : $TASK->get_second_annotation(), $url, $IMAGE_WIDTH, $TASK->reconciled_task->get_name()); ?>
	  </td>
         
	  <td style="width: 200px">
            <input class="neither_button" type="submit" name="neither" value="neither" /><br />
            (e.g. none is good; also click if one annotation is better but still not good enough)
          </td>
     
	  <td>
          <?php annotation_td(3-$displaying_order, $displaying_order==1 ? $TASK->get_second_annotation() : $TASK->get_first_annotation(), $url, $IMAGE_WIDTH, $TASK->reconciled_task->get_name()); ?>
	  </td>
	
	
	  <!-- Bounding box annotator -->
	  <script type="text/javascript">
    $(document).ready(function() {
    $("#tweet_image").load(function(){
         s = {w:this.naturalWidth, h:this.naturalHeight};
         scaling_factor = <?php echo $IMAGE_WIDTH; ?>/s.w;

              <?php // Add relevant bounding boxes of annotations
              foreach(json_decode($TASK->get_first_annotation()['bounding_boxes']) as $bbox) {
                if ($bbox->label==strtolower($TASK->get_fields()['concept']->get_value()))
                  echo 'new_bb($("#bbox_annotator1"), "'.$bbox->label .'", '.$bbox->left .'*scaling_factor, '.$bbox->top .'*scaling_factor, '.($bbox->left + $bbox->width).'*scaling_factor-1, '.($bbox->top + $bbox->height).'*scaling_factor-1); ';
              }
              foreach(json_decode($TASK->get_second_annotation()['bounding_boxes']) as $bbox) {
                if ($bbox->label==strtolower($TASK->get_fields()['concept']->get_value()))
                  echo 'new_bb($("#bbox_annotator2"), "'.$bbox->label .'", '.$bbox->left .'*scaling_factor, '.$bbox->top .'*scaling_factor, '.($bbox->left + $bbox->width).'*scaling_factor-1, '.($bbox->top + $bbox->height).'*scaling_factor-1); ';
              }
              ?>
      }); 

    });
	  </script>
  </tr>

  </table>

  <?php
  // hidden fields
  $TASK->get_fields()['id']->display();
  $TASK->get_fields()['item_id']->display();
  $TASK->get_fields()['annotation1_id']->display();
  $TASK->get_fields()['annotation2_id']->display();
  $TASK->get_fields()['reconciled_task']->display();
  $TASK->get_fields()['concept']->display();
  $TASK->get_fields()['conflict_type']->display();
  $TASK->get_fields()['annotator']->display(); ?>
  <input type="hidden" name="annotator_pass" value="<?php echo $USERS[$annotator]['password']; ?>" />

</form>

<?php endif; // $item could be read ?>
