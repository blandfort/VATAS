<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

/***************************************************************************

    ANNOTATION OVERVIEW PAGE

***************************************************************************/


// Include displaying functions
include_once(FUNCTION_PATH.'displaying.php');

// Regrouping by item ID
function add_annotations($annotations, $new_annotations, $annotation_type) {
  foreach ($new_annotations as $ann) {
    if (!array_key_exists($ann['item_id'], $annotations)) {
      $annotations[$ann['item_id']] = array("annotations"=>array(), "expert_annotations"=>array(),
        "info"=>$ann);
    }
    $annotations[$ann['item_id']][$annotation_type][] = $ann;
  }
  return $annotations;
}


// If a specific task was selected, get the name of the selected task
$task_name = (isset($_GET['task']) and array_key_exists($_GET['task'], $TASKS)) ? $_GET['task'] : null;

?>
<h2>Navigation</h2>
<ul>
	<?php foreach($TASKS as $name=>$task) {
	  if(in_array($name, $RECONCILE_TASKS)) {
	    // skip tasks that are used for reconciling other fields
	    continue;
	  }
	  if(in_array($annotator,$task['users']) or in_array($annotator,$task['experts']) or admin_rights()) { ?>
	  <li><a href="index.php?p=annotations&amp;task=<?php echo $name; ?>"><?php echo $task['title']; ?></a></li>
	<?php }
	} ?>
</ul>

<?php if($task_name):
  // Load annotations
  include_once ABSPATH.'tasks/'.$TASKS[$task_name]['class_name'].'.class.php';
  
  $task = new $TASKS[$task_name]['class_name']($task_name, $annotator);
  $task_experts = $task->settings()['experts'];
  $annotations = array();
  // If logged in user has admin rights, all annotations are read
  // Else, only annotations of the user are shown and listed either under normal or under expert annotations
  $annotations = add_annotations($annotations, $task->load_annotations(admin_rights() ? $task->settings()['users'] : (in_array($annotator,$task_experts) ? array() : array($annotator))), 'annotations');
  $annotations = add_annotations($annotations, $task->load_annotations(admin_rights() ? $task_experts : (in_array($annotator,$task_experts) ? array($annotator) : array())), 'expert_annotations');
  $annotations = add_annotations($annotations, $task->load_annotations(admin_rights() ? $task->settings()['tie_breaker'] : array()), 'reconcile_annotations');
  // Note that annotations of users not belonging to annotators or experts are not included in this overview!
  ?>
  <h2>Annotations for <?php echo $TASKS[$task_name]['title']; ?></h2>

  <?php if(count($annotations)) { ?>
    <table class="annotation_table">
      <tr><th></th><th>Text</th><th>Image</th><th>Annotations</th><th>Conflicts<?php if(array_key_exists('field_reconcile_tasks', $TASKS[$task_name]) and admin_rights()) echo '*'; ?></th><?php if(count($task_experts)) echo '<th>Expert Annotations</th><th>Expert Conflicts</th>'; ?></tr>
      <?php
        $counter = 1;
        foreach($annotations as $item_id=>$r_gr_info) {
          // fields with item content
          echo '<tr><td>'.$counter.'</td><td>'.user_link($r_gr_info['info']['screen_name']).': <a href="'.$r_gr_info['info']['url'].'" target="_blank">'.$r_gr_info['info']['text'].'</a></td><td>';

          if ($r_gr_info['info']['media_url']) {
            $media_url = BASE_URL.'data/'.end((explode('/', $r_gr_info['info']['media_url'])));
            echo '<a href="'.$media_url.'" target="_blank">[show image]</a></td><td>';
          }
          else {
            echo '-</td><td>';
          }

          // annotations of normal annotators
          if (count($r_gr_info['annotations'])) {
            echo '<ul>';
            foreach($r_gr_info["annotations"] as $ann) {
              echo '<li><a href="?p='.$task_name.'&amp;id='.$ann['id'].'">'.(admin_rights()?$ann['annotator']:'view my annotation').'</a></li>';
            }
            echo '</ul>';
          } else {
            echo '-';
          }
          echo '</td><td>';
          
          // disagreements
          if (count($r_gr_info['annotations'])>1) {
            echo '<ul>';
            $disagreements = $task->compute_disagreements($r_gr_info['annotations']);
            foreach($disagreements as $disagreement) {
              // links to annotated fields
              $field_links = array();
              foreach($r_gr_info['annotations'] as $ann) {
                $field_links[] = '<a href="?p='.$task_name.'&amp;id='.$ann['id'].'&amp;field='.$disagreement.'">'.substr($ann['annotator'],0,2).'</a>';
              }
              echo '<li>'.$disagreement.' ('.implode(', ', $field_links).')</li>';
            }
            echo '</ul>';
            
            // provide links for tie breaking
            if(count($r_gr_info['reconcile_annotations'])>0) {
              echo '<a href="?p='.$task_name.'&amp;id='.$r_gr_info['reconcile_annotations'][0]['id'].'&amp;reconcile" style="color:green;">view reconciliation</a>';
            } elseif(in_array($annotator, $task->settings()['tie_breaker']) and count($disagreements)>0) {
              echo '<a href="?p='.$task_name.'&amp;item_id='.$ann['item_id'].'&amp;reconcile" style="color:red;">break ties</a>';
            }
          }
          if (count($r_gr_info['annotations'])<=1 or count($disagreements)<1) {
            echo '-';
          }
          
          if(count($task_experts)) {
            echo '</td><td>';

            // expert annotations
            if (count($r_gr_info['expert_annotations'])) {
              echo '<ul>';
              foreach($r_gr_info["expert_annotations"] as $ann) {
                echo '<li><a href="?p='.$task_name.'&amp;id='.$ann['id'].'">'.(admin_rights()?$ann['annotator']:'view my annotation').'</a></li>';
              }
              echo '</ul>';
            } else {
              echo '-';
            }
            echo '</td><td>';
            
            // expert disagreements
            if (count($r_gr_info['expert_annotations'])>1) {
              echo '<ul>';
              $disagreements = $task->compute_disagreements($r_gr_info['expert_annotations']);
              foreach($disagreements as $disagreement) {
                // links to annotated fields
                $field_links = array();
                foreach($r_gr_info['expert_annotations'] as $ann) {
                  $field_links[] = '<a href="?p='.$task_name.'&amp;id='.$ann['id'].'&amp;field='.$disagreement.'">'.substr($ann['annotator'],0,2).'</a>';
                }
                echo '<li>'.$disagreement.' ('.implode(', ', $field_links).')</li>';
              }
              echo '</ul>';
            }
            if (count($r_gr_info['expert_annotations'])<=1 or count($disagreements)<1) {
              echo '-';
            }
          }
          echo '</td><tr>';

          $counter++;
        }
      ?>

    </table>
    <?php
    // information about separate reconciling tasks
    if(array_key_exists('field_reconcile_tasks', $TASKS[$task_name]) and admin_rights()) {
      ?>
      <p>*<b>Note:</b> For some of the fields there are designated tasks for computing and resolving conflicts:</p><ul>
      <?php foreach($TASKS[$task_name]['field_reconcile_tasks'] as $field_name=>$special_task) {
        echo '<li>'.$field_name.': <a href="index.php?p='.$special_task.'&amp;task='.$task_name.'">'.$TASKS[$special_task]['title'].'</a></li>';
      } ?>
      </ul><p>Conflicts for these fields are <em>not</em> included in the overview above.</p>
      <?php
    }
  } else { // !count($annotations)
    echo '<p><em>No annotations found for this task.</em></p>';
  }

endif; // $task_name ?>
