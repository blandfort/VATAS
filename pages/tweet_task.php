<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

// Include displaying functions
include_once(FUNCTION_PATH.'displaying.php');

if($item): // we only show the form if there is an item for annotation ?>

<form method="POST" action="index.php?p=<?php echo $page['name']; if(isset($FORM_TARGET_APPENDIX)) echo $FORM_TARGET_APPENDIX; ?>" style="margin-top:15px;" onsubmit="return leave_page_<?php echo sizeof($TASK_PAGES); ?>(true_function);">
<script>
  function true_function() {
    return true;
  }
</script>

<?php display_tweet_div($item['url'], $item['screen_name'], $item['text'],
                        $item['created_at'], $url, True, (isset($BBOX_ON_FIRST_PAGE) ? $BBOX_ON_FIRST_PAGE : false)); ?>

<div style="display:inline-block;vertical-align:top; max-width:500px;">
  <?php
  // hidden fields
  $TASK->get_fields()['id']->display();
  $TASK->get_fields()['item_id']->display();
  $TASK->get_fields()['annotator']->display(); ?>
  <input type="hidden" name="annotator_pass" value="<?php echo $USERS[$annotator]['password']; ?>" />


  <?php if(isset($TASK_INSTRUCTIONS)) {
    $TASK_INSTRUCTIONS[0]();
  } ?>
  
  <?php
  // elements to only be displayed on certain task pages
  // (return by task page function)
  $special_page_elements = array();
  // fields displayed on specific pages
  $specific_page_fields = array();
  
  // Display all individual pages
  for($i=0; $i<sizeof($TASK_PAGES); $i++) {
    echo '<div id="page_'.($i+1).'" '.($i==0 ? '' : 'style="display:none"').'>';
    $page_return = $TASK_PAGES[$i]();
    
    $page_has_bbox_annotation = False;
    foreach($page_return as $included_field) {
      if('bounding_boxes'==$included_field->get_name()) {
        $page_has_bbox_annotation = True;
      }
    }
    $specific_page_fields[] = $page_return;
    
    if($page_has_bbox_annotation) {
      $special_page_elements[] = array(array('bbox_annotator'), array('tweet_image'));
    } else {
      $special_page_elements[] = array(array(), array());
    }
    // Navigation to previous and next page of the task
    // and submit button (displayed on last page only)
    if($i==sizeof($TASK_PAGES)-1) {
      // If there is only one page, there needs to be submit button but no back button
      if($i>0) {
        echo '<a id="show_page_'.$i.'" class="page_link" onclick="javascript:show_page_'.$i.'()">Back to Part '.$i.'</a>';
      }
      // When resolving conflicts, the submit button is shown somewhere else
      if(!isset($_GET['reconcile'])) {
        echo '<input name="submit" type="submit" value="Submit" style="float:right" />';
      }
    } elseif($i==0) { ?>
      <a id="show_page_2" style="float:right" class="page_link" onclick="javascript:leave_page_<?php echo $i+1; ?>(show_page_2)">Go to Part 2</a>
      <div style="clear:both;"></div>
    <?php } else {
      echo '<a id="show_page_'.$i.'" class="page_link" onclick="show_page_'.$i.'()">Back to Part '.$i.'</a>';
      echo '<a id="show_page_'.($i+2).'" style="float:right" class="page_link" onclick="javascript:leave_page_'.($i+1).'(show_page_'.($i+2).')">Go to Part '.($i+2).'</a>';
    }
    
    echo '</div>';
  }
  
  // JS code for page navigation
  for($i=0; $i<sizeof($TASK_PAGES); $i++) {
    ?>
    <script>
      function show_page_<?php echo $i+1; ?>() {
        <?php
        for($j=0; $j<sizeof($TASK_PAGES); $j++) {
          echo "document.getElementById('page_".($j+1)."').style.display = '".($i==$j ? 'block' : 'none')."'; ";
          
          // page-specific elements
          foreach($special_page_elements[$j][0] as $pos_elt_id) {
            echo "document.getElementById('".$pos_elt_id."').style.display = '".($i==$j ? 'block' : 'none')."'; ";
          }
          foreach($special_page_elements[$j][1] as $neg_elt_id) {
            echo "document.getElementById('".$neg_elt_id."').style.display = '".($i==$j ? 'none' : 'block')."'; ";
          }
        }
        if(isset($TASK_INSTRUCTIONS)) {
          echo "document.getElementById('".$TASK_INSTRUCTIONS[0]."').style.display = '".(in_array($TASK_PAGES[$i], $TASK_INSTRUCTIONS[1]) ? 'block' : 'none')."'; ";
        }
        ?>
      }
      
      function leave_page_<?php echo $i+1; ?>(leaving_action) {
        // if providing breaking tie annotation, filling in only parts of the form is fine
        <?php if(isset($_GET['reconcile'])) echo 'return leaving_action();'; ?>
      
        // check if all required fields on the page were filled
        <?php foreach($specific_page_fields[$i] as $page_field) { 
          if($page_field->is_required()) { ?>
            if (!$("[name=<?php echo $page_field->get_name(); ?>]").val()) {
              alert("Please fill in all fields before leaving this page. (<?php echo $page_field->get_name(); ?>)");
              return false;
            }
        <?php 
          }
        } ?>
        return leaving_action();
      }
    </script>
    <?php
  }

  // Show submit button here (on every page) when resolving conflicts
  if(isset($_GET['reconcile'])) {
    echo '<div style="text-align:center;"><input name="submit" type="submit" value="Submit" /></div>';
  }
  
// Navigate directly to a specific field if requested
if(isset($_GET['field'])) {
  for($i=0; $i<sizeof($TASK_PAGES); $i++) {
    foreach($specific_page_fields[$i] as $field) {
      if($_GET['field']==$field->get_name()) {
        echo '<script>$(document).ready( function(){ show_page_'.($i+1).'(); });</script>';
        break;
      }
    }
  }
}
?>

</div>

</form>
  
<!-- button to mark user as unavailable -->
<form method="POST" style="margin-top:70px" action="#">
  <input type="hidden" name="user_id" value="<?php echo $item['user_id']; ?>" />
  <input name="unavailable" onclick="return confirm('Are you sure all items of this user are unavailable? Confirming will remove all items from this user.');" type="submit" value="User is deleted or profile is private" />
</form>

<?php
// When logged in as admin and annotation is load by ID, give option to delete
if(admin_rights() and $_GET['id']) { ?>
  <div style="text-align:right;">
    <form method="POST" action="#">
    <?php echo $TASK->get_fields()['id']->display(); ?>   
    <input name="delete" onclick="return confirm('Are you sure you want to delete this annotation? Note that deleting annotations can not be undone.');" type="submit" value="Delete this annotation" />
    </form>
  </div>
<?php } ?>

<?php endif; // $item could be read ?>
