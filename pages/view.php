<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

// Include displaying functions
include_once(FUNCTION_PATH.'displaying.php');


$DATA_TABLE = $TASK->settings()['data_table'];


function structure_data($data) {
  // take data from one user and group it together by ID
  // (Note that each element of $data is expected to contain an annotation.)
  $structured_data = array();
  
  foreach ($data as $dat) {
    if (!array_key_exists($dat['tweet_id'], $structured_data)) {
      $structured_data[$dat['tweet_id']] = array("annotations"=>array(), "info"=>$dat);
    }
    $structured_data[$dat['tweet_id']]['annotations'][] = array("annotator"=>$dat['annotator'], "codes"=>$dat['collapsed_code'], "created_at"=>$dat['created_at']);
  }
  return $structured_data;
}


// If a specific user was selected, store the id of the selected user
$user_id = isset($_GET['user']) ? intval($_GET['user']) : null;


if($user_id):

  $dat = db_elements_from_query("SELECT * FROM ".$DATA_TABLE." WHERE user_id = ".$user_id." ORDER BY tweet_datetime;");
  // group annotations by tweets
  $strucdat = structure_data($dat); 
  
  ?>
  <h2>Posts of user with ID <?php echo $user_id; ?> (<a href="index.php?p=<?php echo $TASK_NAME; ?>">back to overview</a>)</h2>

  <?php if(count($strucdat)) { ?>
    <table class="annotation_table">
      <tr><th></th><th>Posted at</th><th>Text</th><th>Image</th><th>Annotated Codes</th></tr>
      <?php
        $counter = 1;
        foreach($strucdat as $item_id=>$r_gr_info) {
          // fields with item content
          echo '<tr><td>'.$counter.'</td><td>'.$r_gr_info['info']['tweet_datetime'].'</td><td>'.user_link($r_gr_info['info']['screen_name']).': <a href="'.$r_gr_info['info']['url'].'" target="_blank">'.$r_gr_info['info']['text'].'</a></td><td>';

          if ($r_gr_info['info']['media_url']) {
            $media_url = BASE_URL.'data/'.end((explode('/', $r_gr_info['info']['media_url'])));
            echo '<a href="'.$media_url.'" target="_blank">[show image]</a></td><td>';
          }
          else {
            echo '-</td><td>';
          }

          // annotations of normal annotators
          if (count($r_gr_info['annotations'])) {
            echo '<ul>';
            foreach($r_gr_info["annotations"] as $ann) {
              echo '<li>'.($ann['codes'] ? $ann['codes'] : '-').' ('.$ann['annotator'].')</li>';
            }
            echo '</ul>';
          } else {
            echo '-';
          }
          echo '</td><tr>';

          $counter++;
        }
      ?>

    </table>
  <?php
  } else {
    
  }

else:

  $dat = db_get_all_rows($DATA_TABLE);

  // count tweets of the users
  $user_data = array();
  
  foreach($dat as $d) {
    if(!array_key_exists($d['user_id'], $user_data)) {
      $user_data[$d['user_id']] = array('screen_name'=> $d['screen_name'], 'tweets'=>array());
    }
    if(!in_array($d['tweet_id'], $user_data[$d['user_id']]['tweets'])) {
      $user_data[$d['user_id']]['tweets'][] = $d['tweet_id'];
    }
  }

  ?>
  <h2>Relevant users</h2>
  
  <ul>
  <?php foreach($user_data as $u=>$stuff) {
    echo '<li><a href="index.php?p='.$TASK_NAME.'&amp;user='.$u.'">'.$stuff['screen_name'].'</a> ('.sizeof($stuff['tweets']).' tweets)</li>';
  } ?>
  </ul>
  
  <p><em>Note: Only tweets with at least one annotation are considered.</em></p>

  <?php
endif; // $user_id ?>
