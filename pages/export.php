<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');
?>

<h2>Export as CSV</h2>
<ul>
	<?php foreach($TASKS as $name=>$task) {
	  if(in_array($annotator,$task['users']) or admin_rights()) { ?>
	  <li><form method="POST" action="index.php?p=<?php echo $name; ?>">
	    <input type="submit" name="export" value="<?php echo $task['title']; ?>" />
	  </form></li>
	<?php }
	} ?>
</ul>
