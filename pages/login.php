<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');
?>
<div class="container">


	<div class="row col-sm-4 col-md-offset-4">
		<form method="POST">
			<h2>VATAS</h2>
			

			<div class="form-group">
				<label for="inputUsername">Username</label>
				<input id="inputUsername" type="text" name="username" class="form-control" placeholder="Username" required>
			</div>
			
			<div class="form-group">
				<label for="inputPassword">Password</label>
				<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
			</div>
			

			<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
		</form>

		<?php
			if (isset($_POST['username']) and isset($_POST['password'])) {

				$username = $_POST['username'];
				$password = $_POST['password'];
				
				if (array_key_exists($username,$USERS) and md5($password)===$USERS[$username]['password']) {
                                        ini_set('session.gc_maxlifetime', 7200);
			                session_start(['cookie_lifetime' => 7200]);
				        $_SESSION['annotator'] = $username;
				        $_SESSION['system'] = 'images';
					header('Location: index.php?p=home') ;
					exit();
				}
				else {
					echo "Invalid login credentials. Please, try again!";
				}
			}
		?>

	</div>

</div>
