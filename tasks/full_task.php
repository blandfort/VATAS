<?php

/***************************************************************************

    MAIN FILE OF THE TASK main_task

***************************************************************************/


/* DEFINING THE TASK */
$task_class = $TASKS[$TASK_NAME]['class_name'];
include_once($task_class.'.class.php');
$TASK = new $task_class($TASK_NAME, $annotator);


/* PROCESSING REQUESTS AND LOADING ANNOTATION DATA */
include_once('generic/task_processing.php');


/* LAYOUT OF THE ANNOTATION TASK */

// Our task contains several pages, for each one we define a function that displays the page contents
// and return the fields that are included on the page.
function page_0() {
  ?>
  <div id="page_0" style="display:none;">
  <h1>Context</h1>
  <p>Before you answer the questions below, make sure you understand the tweet and are aware of its context:</p>
  <ol>
    <li>Look at the text (emojis, hashtags)</li>
    <li>Look at the image</li>
    <li>Look at any other links in the tweet</li>
    <li>Go to the original post on Twitter<ol>
      <li>See who the poster is</li>
      <li>If anyone is @ed, see who they are</li>
      <li>See if any offline events are referenced</li>
      <li>See the amount of likes and retweets</li>
      <li>See if there are any replies/comments on the tweet</li>
    </ol></li>
  </ol>
  </div>
  <?php
}

function page_1() {
  global $TASK;
  ?>
  <h1>Part 1/3 &ndash; First Impression</h1>
  <p>If this tweet came up on your social media wall, what would be your first impression? (What would come to your mind right after seeing it? How would it make you feel?)<br />
  <?php $TASK->get_fields()['first_impression']->display(); ?></p>

  <?php
  return array($TASK->get_fields()['first_impression']);
}

function page_2() {
  global $TASK;
  ?>
  <h1>Part 2/3 &ndash; Analysis</h1>
  <h2>General Description</h2>
  <p>Please describe in detail everything that is going on in this tweet.</p>
  <?php $TASK->get_fields()['comments']->display(); ?>

  <h2>Threat-Level</h2>
  <p>We are interested in understanding threats in tweets. Can you comment on the level of threat in this tweet? (To answer this question please consider the image and the text of the tweet.)</p>
  <?php $TASK->get_fields()['threat_level']->display(); ?>

  <p>Why did you score the way you did? Please explain in detail:<br />
  <?php $TASK->get_fields()['threat_explanation']->display(); ?></p>

  <?php 
  return array($TASK->get_fields()['comments'], $TASK->get_fields()['threat_level'], $TASK->get_fields()['threat_explanation']);
}

function page_3() {
  global $TASK;
  ?>
  <h1>Part 3/3 &ndash; Code</h1>
  <?php $TASK->get_fields()['communication_code']->display();
  
  return array($TASK->get_fields()['communication_code']);
}

// The following arrays are used by the page template to compile the annotation page:
$TASK_INSTRUCTIONS = array('page_0', array('page_2', 'page_3')); // general element, sub-array specifies on which pages it is visible
// task pages should return an array of fields displayed on the page
$TASK_PAGES = array('page_1', 'page_2', 'page_3');

?>
