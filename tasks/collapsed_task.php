<?php

/***************************************************************************

    MAIN FILE OF THE TASK main_task

***************************************************************************/


/* DEFINING THE TASK */
$task_class = $TASKS[$TASK_NAME]['class_name'];
include_once($task_class.'.class.php');
$TASK = new $task_class($TASK_NAME, $annotator);


/* PROCESSING REQUESTS AND LOADING ANNOTATION DATA */
include_once('generic/task_processing.php');


/* LAYOUT OF THE ANNOTATION TASK */

function page_1() {
  global $TASK;
  ?><!--
  <h1>Context</h1>
  <p>Before you answer the next question, make sure you understand the tweet and are aware of its context:</p>
  <ol>
    <li>Look at the text (emojis, hashtags)</li>
    <li>Look at the image</li>
    <li>Look at any other links in the tweet</li>
    <li>Go to the original post on Twitter<ol>
      <li>See who the poster is</li>
      <li>If anyone is @ed, see who they are</li>
      <li>See if any offline events are referenced</li>
      <li>See the amount of likes and retweets</li>
      <li>See if there are any replies/comments on the tweet</li>
    </ol></li>
  </ol>-->

  <h1>Code</h1>
  <?php $TASK->get_fields()['collapsed_code']->display();
  
  return array($TASK->get_fields()['collapsed_code']);
}

// The following array is used by the page template to compile the annotation page:
// task pages should return an array of fields displayed on the page
$TASK_PAGES = array('page_1');

?>
