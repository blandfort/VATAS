<?php

/***************************************************************************

    MAIN FILE OF THE TASK analysis_task

***************************************************************************/


/* DEFINING THE TASK */
$task_class = $TASKS[$TASK_NAME]['class_name'];
include_once($task_class.'.class.php');
$TASK = new $task_class($TASK_NAME, $annotator);

?>
