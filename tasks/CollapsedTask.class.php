<?php

// Basic task class
include_once('generic/BaseTask.class.php');

// Include field classes
include_once('generic/fields.php');

// Loading additional resources
include_once('generic/codes.php'); // collapsed codes


// Define the main class for the task
class CollapsedTask extends BaseTask {
  function __construct($task_name, $annotator) {
    parent::__construct($task_name, $annotator);
    
    // task-specific fields
    // (names given to Field constructors must correspond to database columns)
    $task_fields = array(
      'collapsed_code'=>new CheckListField('collapsed_code', collapsed_codes()),
    );
    foreach($task_fields as $field_name=>$field) {
      $this->set_field($field_name, $field);
    }
  }
}

?>
