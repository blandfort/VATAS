<?php

/***************************************************************************

    MAIN FILE OF THE TASK bbox_task
    
    This task is used to annotate bounding boxes for tweet images.

***************************************************************************/


/* DEFINING THE TASK */
$task_class = $TASKS[$TASK_NAME]['class_name'];
include_once($task_class.'.class.php');
$TASK = new $task_class($TASK_NAME, $annotator);


/* PROCESSING REQUESTS AND LOADING ANNOTATION DATA */
include_once('generic/task_processing.php');


/* LAYOUT OF THE ANNOTATION TASK */

function page_content() {
  global $TASK;
  ?>
  <h1>People and Objects</h1>     
  
  <?php
  $bbox_task = $TASK->get_fields()['bounding_boxes'];
  $bbox_task->display();
  make_bbox_instructions($bbox_task->get_labels(), instruction_files()); ?>

  <p></p>
  <?php
  return array($TASK->get_fields()['bounding_boxes']);
}

// The following array is used by the page template to compile the annotation page:
// task pages should return an array of fields displayed on the page
$TASK_PAGES = array('page_content');

$BBOX_ON_FIRST_PAGE = true; // show bounding box annotator right away

?>
