<p><small><em>Note: For illustration we are using the flickr image "Sighted" by Mike Beltzner.</em></small></p>
<tr>
  <td class="col-md-4"><mark><b>Bounding boxes should be as tight as possible and include the whole object/person.</b></mark></td>
  <td class="success col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/ok.png" style="position:absolute; top:0" /><img width="100%" class="img-rounded center-block" src="<?php echo IMAGE_DIR_URL; ?>instructions/general_tight_correct.jpg"/></div>
  <td class="danger col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/no.png" style="position:absolute; top:0" /><img width="100%" class="img-rounded center-block" src="<?php echo IMAGE_DIR_URL; ?>instructions/general_tight_wrong.jpg"/></div>
</tr>

