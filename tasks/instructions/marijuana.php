<p><small><em>Note: For illustration we are using the flickr images (from left to right and top to bottom) "nugs" by SWARE., "Joint Stuff" by Heath Alseike, "Marijuana" by Paige Filler and "I don't think this plant will be here long." by Philip Bump.</em></small></p>
<tr>
  <td class="col-md-4"><mark><b>Examples for marijuana</b></mark></td>
  <td class="success col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/ok.png" style="position:absolute; top:0" /><img width="100%" class="img-rounded center-block" src="<?php echo IMAGE_DIR_URL; ?>instructions/marijuana.jpg"/></div>
  <td class="success col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/ok.png" style="position:absolute; top:0" /><img width="100%" class="img-rounded center-block" src="<?php echo IMAGE_DIR_URL; ?>instructions/marijuana_bag.jpg"/></div>
</tr>

<tr>
  <td class="col-md-4">
    <p><mark><b>Do not mark intact leafs or marijuana plants.</b></mark></p>
  </td>
  <td class="danger col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/no.png" style="position:absolute; top:0" /><img width="100%" class="img-rounded" src="<?php echo IMAGE_DIR_URL; ?>instructions/marijuana_plant.jpg" /></div></td>
  <td class="danger col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/no.png" style="position:absolute; top:0" /><img  width="100%" class="img-rounded" src="<?php echo IMAGE_DIR_URL; ?>instructions/marijuana_washington.jpg" /></div></td>
</tr>

