<p><small><em>Note: For illustration we are using the flickr images (from left to right and top to bottom) "Sighted" by Mike Beltzner, "P2040189" by Mike Petrucci, "Graffiti gun" by Kostya Vacuum and "Nerf Sniper Rifle" by Gamerscore Blog.</em></small></p>
<tr>
  <td class="col-md-4"><p><mark><b>Examples for firearm.</b></mark></p></td>
  <td class="success col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/ok.png" style="position:absolute; top:0" /><img class="img-rounded" src="<?php echo IMAGE_DIR_URL; ?>instructions/handgun.jpg" width="100%"/></div></td>
  <td class="success col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/ok.png" style="position:absolute; top:0" /><img class="img-rounded" src="<?php echo IMAGE_DIR_URL; ?>instructions/rifle.jpg" width="100%"/></div></td>
</tr>
<tr>
  <td class="col-md-4"><p><mark><b>Only annotate real firearms.</b></mark></p>
    <p>In particular, do not mark drawings of weapons or toy guns.</p>
  </td>
  <td class="danger col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/no.png" style="position:absolute; top:0" /><img class="img-rounded" src="<?php echo IMAGE_DIR_URL; ?>instructions/graffiti_gun.jpg" width="100%"/></div></td>
  <td class="danger col-md-4"><div style="position:relative; left:0; top:0"><img class="overlay-icon" src="<?php echo BASE_URL; ?>tasks/instructions/no.png" style="position:absolute; top:0" /><img class="img-rounded" src="<?php echo IMAGE_DIR_URL; ?>instructions/toy_gun.jpg" width="100%"/></div>
    </td>
</tr>

