<?php

// Basic task class
include_once('generic/BaseTask.class.php');

// Include field classes
include_once('generic/fields.php');

// Loading additional resources
include_once('generic/codes.php'); // communication codes


// Define the main class for the task
class CodeTask extends BaseTask {
  function __construct($task_name, $annotator) {
    parent::__construct($task_name, $annotator);
    
    // task-specific fields
    // (names given to Field constructors must correspond to database columns)
    $task_fields = array(
      'first_impression'=>new TextAreaField('first_impression'),
      'communication_code'=>new CheckListField('communication_code', communication_codes()),
    );
    foreach($task_fields as $field_name=>$field) {
      $this->set_field($field_name, $field);
    }
  }
}

?>
