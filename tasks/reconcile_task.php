<?php

/***************************************************************************

    MAIN FILE OF THE TASK reconcile_task
    
    This task is used to reconcile bounding box annotations
    of the task main_task.

    #TODO bounding box creation for neither cases should be limited to the relevant concept only

***************************************************************************/


/* DEFINING THE TASK */
$task_class = $TASKS[$TASK_NAME]['class_name'];
include_once($task_class.'.class.php');
// set task to reconcile (if specified)
if(isset($_GET['task']) and array_key_exists($_GET['task'], $TASKS)) {
  $TASK = new $task_class($TASK_NAME, $annotator, $_GET['task']);
} else {
  $TASK = new $task_class($TASK_NAME, $annotator);
}


/* PROCESSING REQUESTS */

// If the form was submitted, write the response to the database.
if(isset($_POST["vote1"]) or isset($_POST["vote2"]) or isset($_POST['neither'])) {
  $vote = 0;
  if(isset($_POST["vote1"])) {
    $vote = -1;
  }
  if(isset($_POST['vote2'])) {
    $vote = 1;
  }
  $_POST['decision'] = $vote;
  $TASK->store_annotation($_POST);
}

// If bounding boxes were given for neither case, write the response to the database.
if(isset($_POST["submit"])) {
  if(isset($_POST["id"]) and $_POST["id"]) {
    $TASK->update_annotation($_POST);
  } else {
    $existing_user_annotation = $TASK->load_annotation_by_item_id($_POST['item_id'], $annotator);
    if($existing_user_annotation) {
      // If entry for this user and tweet already exists, we update the existing response
      $_POST["id"] = $existing_user_annotation['id'];
      $TASK->update_annotation($_POST);
    } else {
      $TASK->store_annotation($_POST);
    }
  }
}

// Exporting data
if(isset($_POST['export'])) {
  $TASK->export_as_csv();
}


/* LOADING ANNOTATION DATA */

$fields = $TASK->get_fields();

$annotation_id = null;
$item_id = null;
$annotation1_id = null;
$annotation2_id = null;


// Load the item for annotation.
$conflict = $TASK->load_item();
if ($conflict) {
  $item = $TASK->reconciled_task->load_item_by_id($conflict['item_id']);
  $temp = explode('/', $item['media_url']);
  $url = IMAGE_DIR_URL.end($temp); #TODO make function to convert media_url into path
  $fields['item_id']->set_value($conflict['item_id']);
  $fields['annotation1_id']->set_value($conflict['annotation1_id']);
  $fields['annotation2_id']->set_value($conflict['annotation2_id']);
  $fields['concept']->set_value($conflict['concept']);
  $fields['conflict_type']->set_value($conflict['conflict_type']);


  // For the 'neither' cases, we need to show the bounding box annotation page 
  if(array_key_exists('decision', $conflict) and $conflict['decision']==0) {
    $fields['id']->set_value($conflict['id']);
    $fields['bounding_boxes']->set_image_url($url);
    $fields['bounding_boxes']->set_labels(array($fields['concept']->get_value()));
  
    function page_content() {
      global $TASK;
      ?>
      <h1>People and Objects</h1>     
      
      <?php   
      // extra hidden fields  
      $TASK->get_fields()['annotation1_id']->display();
      $TASK->get_fields()['annotation2_id']->display();
      $TASK->get_fields()['reconciled_task']->display();
      $TASK->get_fields()['concept']->display();
      $TASK->get_fields()['conflict_type']->display();
  
      $bbox_task = $TASK->get_fields()['bounding_boxes'];
      $bbox_task->display();
      make_bbox_instructions($bbox_task->get_labels(), instruction_files()); ?>

      <p></p>
      <?php
      return array($TASK->get_fields()['bounding_boxes']);
    }

    $TASK_PAGES = array('page_content');
    $BBOX_ON_FIRST_PAGE = true; // show bounding box annotator right away
    $TASKS[$TASK_NAME]['page'] = 'tweet_task';
    $FORM_TARGET_APPENDIX = '&amp;task='.$TASK->get_fields()['reconciled_task']->get_value();
  }
} else {
  trigger_error("No more items to annotate.", E_USER_WARNING);
}


/* LAYOUT OF THE ANNOTATION TASK */
$IMAGE_WIDTH = 500;

?>
