<?php

// Basic task class
include_once('generic/BaseTask.class.php');

// Include field classes
include_once('generic/fields.php');

// Loading additional resources
include_once('generic/codes.php'); // communication codes and bounding box concepts


// Define the main class for the task
class ImageTask extends BaseTask {
  function __construct($task_name, $annotator) {
    parent::__construct($task_name, $annotator);
    
    // task-specific fields
    // (names given to Field constructors must correspond to database columns)
    $task_fields = array(
      'first_impression'=>new TextAreaField('first_impression'),
      'location'=>new RadioField('location', array('indoor'=>'indoor',
                                                                      'outdoor'=>'outdoor',
                                                                      'other'=>'other (e.g. for text-only images or ambiguous cases)')),
      'comments'=>new TextAreaField('comments', 10),
      'location_code'=>new TextField('location_code'),
      'location_text'=>new TextAreaField('location_text', 5),
      'threat_level'=>new RangeField('threat_level', 'not at all threatening', 'extremely threatening', 0, 1, 0.1, 0, 0.2),
      'threat_explanation'=>new TextAreaField('threat_explanation', 5),
      'lean_likeliness'=>new RangeField('lean_likeliness', 'not at all likely', 'completely likely', 0, 1, 0.1, 0, 0.2),
      'lean_explanation'=>new TextAreaField('lean_explanation', 5),
      'bounding_boxes'=>new BoundingBoxField('bounding_boxes', bbox_concepts()),
      'communication_code'=>new CheckListField('communication_code', communication_codes()),
    );
    $task_fields['lean_explanation']->set_required(False);

    foreach($task_fields as $field_name=>$field) {
      $this->set_field($field_name, $field);
    }
  }
}

?>
