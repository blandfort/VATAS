<?php

/***************************************************************************

    MAIN FILE OF THE TASK main_task

***************************************************************************/


/* DEFINING THE TASK */
$task_class = $TASKS[$TASK_NAME]['class_name'];
include_once($task_class.'.class.php');
$TASK = new $task_class($TASK_NAME, $annotator);


/* PROCESSING REQUESTS AND LOADING ANNOTATION DATA */
include_once('generic/task_processing.php');


/* LAYOUT OF THE ANNOTATION TASK */

// Our task contains several pages, for each one we define a function that displays the page contents
// and return the fields that are included on the page.
function page_2() {
  ?>
  <h1>Context</h1>
  <p>Before you answer the next question, make sure you understand the tweet and are aware of its context:</p>
  <ol>
    <li>Look at the text (emojis, hashtags)</li>
    <li>Look at the image</li>
    <li>Look at any other links in the tweet</li>
    <li>Go to the original post on Twitter<ol>
      <li>See who the poster is</li>
      <li>If anyone is @ed, see who they are</li>
      <li>See if any offline events are referenced</li>
      <li>See the amount of likes and retweets</li>
      <li>See if there are any replies/comments on the tweet</li>
    </ol></li>
  </ol>
  <?php
}

function page_1() {
  global $TASK;
  ?>
  <h1>First Impression</h1>
  <p>If this photo came up on your social media wall, what would be your first impression? (What would come to your mind right after seeing it? How would it make you feel?)<br />
  <?php $TASK->get_fields()['first_impression']->display();
  
  return array($TASK->get_fields()['first_impression']);
}

function page_5() {
  global $TASK;
  ?>
  <h1>Code</h1>
  <?php $TASK->get_fields()['communication_code']->display();
  
  return array($TASK->get_fields()['communication_code']);
}

// The following array is used by the page template to compile the annotation page:
// task pages should return an array of fields displayed on the page
$TASK_PAGES = array('page_1', 'page_2', 'page_5');

?>
