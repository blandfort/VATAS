<?php

// Define the main class for the task
class AnalysisTask {
  function __construct($task_name, $annotator) {
    $this->name = $task_name;
  }
  
  public function settings() {
    global $TASKS;
    return $TASKS[$this->name];
  }
  
  public function load_annotations() {
    return null;
  }
}

?>
