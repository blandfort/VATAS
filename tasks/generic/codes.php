<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

/***************************************************************************

    CODES AND CONCEPTS TO USE FOR SEVERAL TASKS

***************************************************************************/

function collapsed_codes() {
  return array(
    array("Aggression", "aggression", ""),
    array("Loss", "loss", ""),
    array("Substance Use", "substance", ""),
  );
}

function communication_codes() {
  return array(
    array("Grieving", "grieving", "<ul>
      <li>Loss</li>
      <li>Death</li>
      <li>Loneliness/Sadness</li>
      <li>Memorial</li>
      <li>Incarceration</li>
      </ul>"),
    array("Aggression", "aggression", "<ul>
      <li>Insulting/Dissing</li>
      <li>Snitching</li>
      <li>Threatening/Calling Out</li>
      <li>Physical Violence</li>
      <li>Challenges with Authority</li>
      <li>Lack of Trust</li>
      </ul>"),
    array("Social Behavior", "social", "<ul>
      <li>Joke</li>
      <li>Entertain</li>
      <li>Conversation</li>
      <li>Deleted</li>
      </ul>"),
    array("Mood", "mood", "<ul>
      <li>Rap Lyrics</li>
      <li>General Information/Statement</li>
      <li>Reflection</li>
      <li>Complaints about Social Media</li>
      <li>Happiness</li>
      </ul>"),
    array("Neighborhood (General/Gang-Related)", "neighborhood", "<ul>
      <li>Neighborhood interactions</li>
      <li>Neighborhood Identity</li>
      <li>Support/Pride</li>
      <li>Unspecified Gang Activity</li>
      </ul>"),
    array("Status Seeking", "status", "<ul>
      <li>Posturing</li>
      <li>Appearance</li>
      <li>Bragging</li>
      <li>Power</li>
      <li>Guns</li>
      <li>Promotional</li>
      <li>Money</li>
      <li>Selling Drugs</li>
      </ul>"),
    array("Health", "health", "<ul>
      <li>Decisions around health</li>
      <li>Health</li>
      <li>Sleep</li>
      <li>Mental</li>
      <li>Eating Behaviors</li>
      <li>Pregnancy</li>
      </ul>"),
    array("Relationships (Romantic/Familial)", "relationship", "<ul>
      <li>Women</li>
      <li>Men</li>
      <li>Family</li>
      <li>Friendships</li>
      <li>Sex</li>
      <li>Love</li>
      </ul>"),
    array("Support", "support", "<ul>
      <li>Reaching Out</li>
      <li>Feeling Unsupported</li>
      <li>Surrounding self with positive motivated people</li>
      <li>Supporting Family</li>
      <li>Community</li>
      <li>Financial</li>
      </ul>"),
    array("Growth", "growth", "<ul>
      <li>Goals</li>
      <li>Motivation</li>
      <li>Transformation</li>
      <li>Improvement</li>
      <li>Moving forward not backward</li>
      <li>Future Intentions</li>
      </ul>"),
    array("Authenticity", "authenticity", "<ul>
      <li>Posts about being real</li>
      <li>Raising Consciousness</li>
      <li>Lacking authenticity</li>
      <li>Individual or Collective</li>
      </ul>"),
    array("Substance Use", "substance", ""),
  );
}

// bounding box concepts
function bbox_concepts() {
  return array(
    'person', 'money',
    'firearm',
    'lean container', 'joint/blunt/cigarette', 'marijuana',
    'hand gesture', 'tattoo',
  );
}

function instruction_files() {
  return array('firearm'=>'firearm.php', 'marijuana'=>'marijuana.php');
}
?>
