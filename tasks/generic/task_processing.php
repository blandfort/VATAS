<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

/*
 * Functions that are executed on pages of individual annotation task pages
 * It is assumed, that $TASK contains an instance of the task class and is included already.
 */

$fields = $TASK->get_fields();


/* PROCESSING REQUESTS */

// If the form was submitted, write the response to the database.
if(isset($_POST["submit"])) {
  if(isset($_POST["id"]) and $_POST["id"]) {
    $TASK->update_annotation($_POST);
  } else {
    $existing_user_annotation = $TASK->load_annotation_by_item_id($_POST['item_id'], $annotator);
    if($existing_user_annotation) {
      // If entry for this user and tweet already exists, we update the existing response
      $_POST["id"] = $existing_user_annotation['id'];
      $TASK->update_annotation($_POST);
    } else {
      $TASK->store_annotation($_POST);
    }
  }
}

// Deleting individual annotations
if(isset($_POST['id']) and isset($_POST['delete'])) {
  $TASK->delete_annotation($_POST['id']);
}

// Button for stating that user is not available anymore
if(isset($_POST["unavailable"])) {
  if(array_key_exists('user_table', $TASK->settings())
    and db_update_entry($TASK->settings()['user_table'], array('available'=>sanitize('f')), array('user_id'=>intval($_POST['user_id'])))) {
    status_message("Successfully set user status to unavailable. You will not be shown any other tweets from this user.");
  } else {
    trigger_error("Could not write information to database.", E_USER_ERROR);
  }
}

// Exporting data
if(isset($_POST['export'])) {
  $TASK->export_as_csv();
}


/* LOADING ANNOTATION DATA */

$annotation_id = null;
$item_id = null;

// Load annotations (if requested)
if (isset($_GET['id']) or isset($_GET['item_id'])) {
  if(isset($_GET['id'])) {
    $annotation_id = intval($_GET['id']);
    $annotation = $TASK->load_annotation($annotation_id);
  } else {
    $item_id = intval($_GET['item_id']);
    $annotation = $TASK->load_annotation_by_item_id($item_id, $annotator);
  }
  if ($annotation) {
    foreach($annotation as $field_name=>$field_value) {
      if(array_key_exists($field_name, $fields)) {
        $fields[$field_name]->set_value($field_value);
      }
    }
  }
}

// Load the item for annotation.
$item = $TASK->load_item($annotation_id, $item_id);
if ($item) {
  if(array_key_exists('media_url', $item) and $item['media_url']) {
    $temp = explode('/', $item['media_url']);
    $url = IMAGE_DIR_URL.end($temp); #TODO make function to convert media_url into path
  } else {
    $url = null;
  }
  if(array_key_exists('bounding_boxes', $fields)) {
    $fields['bounding_boxes']->set_image_url($url); #TODO field could be named differently
  }
  $fields['item_id']->set_value($item['item_id']);
} else {
  trigger_error("No more items to annotate.", E_USER_WARNING);
}

?>
