<?php

/***************************************************************************

    BASE TASK
    
    Most task classes can start from this class.
    So this is like the abstract father of most
    the present and future tasks you will encounter.

***************************************************************************/

// Include field classes
include_once('fields.php');


abstract class BaseTask {
  protected $name; // used to find the right settings
  protected $annotator;
  
  private $fields = array();
  
  public function set_field($name, $field) {
    $this->fields[$name] = $field;
  }
  
  public function get_fields() {
    return $this->fields;
  }
  
  public function set_fields($fields) {
    $this->fields = $fields;
  }
  
  function __construct($name, $annotator) {
    $this->set_fields(array('id'=>new HiddenIntField('id',0), // annotation ID
                                'item_id'=>new HiddenIntField('item_id',null), // item ID
                                'annotator'=>new HiddenField('annotator',$annotator)));
    $this->name = $name;
    $this->annotator = $annotator;
  }
  
  public function get_name() {
    return $this->name;
  }

  public function settings() {
    global $TASKS;
    return $TASKS[$this->name];
  }
  
  
  /* LOADING DATA */
  
  public function load_item_by_id($item_id) {
    return db_get_row_where_values($this->settings()['data_table'], array("item_id"=>intval($item_id)));
  }
  
  public function load_item_for_annotation($annotation_id) {
    $data_table = $this->settings()['data_table'];
    $ann_table = $this->settings()['annotation_table'];

    return db_element_from_query("SELECT ".$data_table.".*
        FROM ".$data_table.", ".$ann_table."
        WHERE ".$data_table.".item_id=".$ann_table.".item_id AND ".$ann_table.".id=".intval($annotation_id).";");
  }
  
  public function load_next_item($position_threshold=-1) {
    // Get the next item to annotate
    $data_table = $this->settings()['data_table'];
    $ann_table = $this->settings()['annotation_table'];
    $user_table = isset($this->settings()['user_table']) ? $this->settings()['user_table'] : '';
    
    // The following restrictions apply:
    // - Current annotator hasn't already annotated the item
    // - The total number of annotations for the item is less than the number specified in the settings
    //   (Note that there are different settings for experts and normal users.)
    // - If a user table is associated with the data, only data from available users is read.
    $sql = "
        SELECT d.*
        FROM ".$data_table." AS d";
    if($user_table) {
      $sql .= " LEFT JOIN (SELECT * FROM ".$user_table." WHERE available=true) AS t ON t.user_id=d.user_id";
    }
    $sql .= " LEFT JOIN (SELECT annotator, item_id FROM ".$ann_table." WHERE ".db_membership_string('annotator', in_array($this->annotator,$this->settings()['experts']) ? $this->settings()['experts'] : $this->settings()['users']).")
            AS r ON d.item_id=r.item_id
          LEFT JOIN (SELECT annotator, item_id FROM ".$ann_table." WHERE annotator=".sanitize($this->annotator).") AS own_r ON d.item_id=own_r.item_id ";
    if(DB_TYPE=='psql') { #NOTE: item_id should be primary key
      $sql .= "GROUP BY d.item_id ";
      if($user_table) $sql .= ", t.position ";
    }
    $sql .= "HAVING count(r.annotator)<".$this->settings()[in_array($this->annotator,$this->settings()['experts']) ? 'expert_annotations_per_item' : 'annotations_per_item']." AND count(own_r.annotator)<1 ";
    if($user_table) {
      $sql .= "AND count(t.user_id)>0 ";
    }
    // We order by user if available, secondly by creation date of the data item
    $sql .= " ORDER BY ".($user_table ? 't.position, ' : '')."created_at;";

    return db_element_from_query($sql);
  }
  
  public function load_item($annotation_id=null, $item_id=null, $position_threshold=-1) {
    if (!$annotation_id and !$item_id
        and (in_array($this->annotator, $this->settings()['users']) or in_array($this->annotator, $this->settings()['experts']) or admin_rights())) {
      return $this->load_next_item($position_threshold);
    } elseif($item_id) {
      return $this->load_item_by_id($item_id);
    } elseif($annotation_id) {
      return $this->load_item_for_annotation($annotation_id);
    } else {
      return '';
    }
  }
  
  
  /* LOADING ANNOTATIONS */

  #FIXME: exclude unavailable users (or mark them clearly in overview)  
  public function load_annotations($annotators=null) {   
    if ($annotators===null) {
      $annotators = array_merge($this->settings()['users'], $this->settings()['experts']);
    }
    $sql = "
      SELECT r.*, d.*
      FROM ".$this->settings()['annotation_table']." AS r, ".$this->settings()['data_table']." AS d
      WHERE r.item_id=d.item_id AND ".db_membership_string('r.annotator', $annotators)."
        ORDER BY r.created_at ASC;";
    return db_elements_from_query($sql);
  }
  
  public function load_annotation($annotation_id) {
    $values = array("id"=>intval($annotation_id));
    
    // only admins are allowed to see others' annotations
    if (!admin_rights()) {
      $values['annotator'] = sanitize($this->annotator);
    }
    return db_get_row_where_values($this->settings()['annotation_table'], $values);
  }
  
  public function load_annotation_by_item_id($item_id, $annotator) {
    return db_get_row_where_values($this->settings()['annotation_table'], array("item_id"=>intval($item_id), 'annotator'=>sanitize($annotator)));
  }
  
  
  /* SAVING AND UPDATING ANNOTATIONS */
  
  // writing annotations to database
  public function store_annotation($post) {
    $entries = array();
    foreach($this->get_fields() as $field) {
      if($field->get_name()=='id') {
        continue;
      }
    
      $field_names[] = $field->get_name();
      if(in_array($field->get_name(), array_keys($post))) {
        $entries[$field->get_name()] = $field->db_value($post[$field->get_name()]);
      } else {
        // Set default value for the field if nothing was specified
        $entries[$field->get_name()] = $field->db_value($field->get_value());
      }
    }
    
    return db_store_entry($this->settings()['annotation_table'], $entries);
  }

  // updating annotations
  public function update_annotation($post) {
    $fields = $this->get_fields();

    $entries = array();
    foreach($fields as $field) {
      $field_names[] = $field->get_name();
      if(in_array($field->get_name(), array_keys($post))) {
        $entries[$field->get_name()] = $field->db_value($post[$field->get_name()]);
      } else { // Note that this is required to uncheck checkboxes but for some tasks you might want to ignore the fields that were not specified
        // Set default value for the field if nothing was specified
        $entries[$field->get_name()] = $field->db_value($field->get_value());
      }
    }
      
    $where_values = array("id"=>intval($post['id']));
    // only admins are allowed to change others' annotations
    if (!admin_rights()) {
      $where_values['annotator'] = sanitize($this->annotator);
    }

    return db_update_entry($this->settings()['annotation_table'], $entries, $where_values);
  }
  
  /* DELETING ANNOTATIONS */
  
  public function delete_annotation($annotation_id) {
    // only admins are allowed to delete
    if(!admin_rights()) {
      trigger_error("Insufficient rights!", E_USER_ERROR);
      return false;
    }
    return db_delete_entry($this->settings()['annotation_table'], $annotation_id);
  }
  
  
  /* EXPORTING INFORMATION */
  
  function export_as_csv() {
    // Checking for rights
    if( !admin_rights() )
      die('Insufficient rights!');
    
    $annotations = $this->load_annotations();

    if(is_array($annotations) and count($annotations)>0) {
      $first_item = $annotations[0];
      $header = array_keys($first_item);

      // output headers so that the file is downloaded rather than displayed
      header('Content-Type: text/csv; charset=utf-8');
      header('Content-Disposition: attachment; filename='.date("Y-m-d_").$this->name.'.csv');

      $fp = fopen('php://output', 'w');
      fputcsv($fp, $header);
      foreach($annotations as $annotation) {
        fputcsv($fp, $annotation);
      }

      fclose($fp);
      exit('');
    } elseif(is_array($annotations)) {
      trigger_error("No items to export!", E_USER_WARNING);
      return;
    } else {
      trigger_error("Couldn't export!", E_USER_ERROR);
      return;
    }
  }
  
  
  /* COMPUTING DISAGREEMENTS */
  
  // take several annotations for a single item and return field names where opinions are diverging
  function compute_disagreements($annotations, $ignore_special=true) {
    $disagreement_fields = array();
    foreach($this->get_fields() as $field) {
      // disagreement doesn't make sense for some special fields
      if(in_array($field->get_name(), array('id', 'annotator'))) {
        continue;
      }
      
      // if requested, skip fields that have special reconciling tasks
      if($ignore_special and array_key_exists($field->get_name(), $this->settings()['field_reconcile_tasks'])) {
        continue;
      }
    
      $field_values = array();
      foreach($annotations as $annotation) {
        $field_values[] = $annotation[$field->get_name()];
      }
      if($field->check_conflict($field_values)) {
        $disagreement_fields[] = $field->get_name();
      }
    }
    return $disagreement_fields;
  }
}

?>
