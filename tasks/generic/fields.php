<?php

/***************************************************************************

    FIELD CLASSES
    
    Use these classes to define fields for your tasks.

***************************************************************************/

#TODO likert scale should be there too

// All fields (except the abstract BaseField) should implement this interface:
interface IField {
  public function get_name(); // for all fields this name should correspond to the corresponding header of the database column
  public function set_value($value);
  public function get_value();
  public function display();
  public function db_value($value); // converting value to format that can be used for database queries
  public function check_conflict($values); // given array of values, check if there is disagreement (return boolean)
}


abstract class BaseField {
  private $name;
  protected $value;
  private $ignore_conflict = False; // if True, always return False when computing disagreement
  private $required = True;
  
  function __construct($name) {
    $this->name = $name;
  }
  
  public function get_name() {
    return $this->name;
  }

  public function set_value($value) {
    $this->value = $value;
  }
  
  public function get_value() {
    return $this->value;
  }
  
  public function db_value($value) {
    return sanitize($value);
  }
  
  public function is_required() {
    return $this->required;
  }
  
  public function set_required($value) {
    $this->required = (boolean)$value;
  }
  
  public function is_ignore_conflict() {
    return $this->ignore_conflict;
  }
  
  public function set_ignore_conflict($ignore) {
    $this->ignore_conflict = (boolean)$ignore;
  }
  
  public function check_conflict($values) {
    if (count($values)<2 or $this->is_ignore_conflict()) {
      return False;
    }
    // pairwise check if there is disagreement
    for($i=0; $i<count($values); $i++) {
      for($j=$i+1; $j<count($values); $j++) {
        if(!$this->compare_values($values[$i],$values[$j])) return True;
      }
    }
    return False;
  }
  
  // check if two field values are considered to be the same (used for computing disagreement)
  public function compare_values($value1, $value2) {
    if($value1==$value2) {
      return True;
    } else {
      return False;
    }
  }
}


class HiddenField extends BaseField implements IField {

  function __construct($name, $defaultvalue='') {
    parent::__construct($name);
    $this->value = $defaultvalue;
  }
  
  public function display() {
    echo '<input type="hidden" name="'.$this->get_name().'" value="'.$this->get_value().'" />';
  }
}

class HiddenIntField extends HiddenField implements IField {

  public function set_value($value) {
    $this->value = (int)$value;
  }
  
  public function db_value($value) {
    return (int)$value;
  }
}


class TextField extends BaseField implements IField {
  private $max_length;
  
  public function get_max_length() {
    return $this->max_length;
  }

  function __construct($name, $max_length=100, $ignore_conflict=True) {
    parent::__construct($name);
    $this->value = '';
    $this->max_length = (int)$max_length;
    $this->set_ignore_conflict($ignore_conflict);
  }
  
  public function display() {
    echo '<input type="text" id="'.$this->get_name().'_field" name="'.$this->get_name().'" value="'.$this->get_value().'" maxlength="'.$this->get_max_length().'" />';
  }
}


class TextAreaField extends BaseField implements IField {
  private $rows;
  
  public function get_rows() {
    return $this->rows;
  }

  function __construct($name, $rows=5, $ignore_conflict=True) {
    parent::__construct($name);
    $this->value = '';
    $this->rows = (int)$rows;
    $this->set_ignore_conflict($ignore_conflict);
  }
  
  public function display() {
    echo '<textarea name="'.$this->get_name().'" id="'.$this->get_name().'_field" rows="'.$this->get_rows().'">'.$this->get_value().'</textarea></p>';
  }
}


#FIXME: required setting for radio fields is not yet working (for location field in demo implementation it is possible to annotate without entering a value)
class RadioField extends BaseField implements IField {
  private $options;
  
  function __construct($name, $options) {
    parent::__construct($name);
    $this->options = $options;
  }
  
  public function get_options() {
    return $this->options;
  }
  
  public function display() {
    echo '<ul style="list-style:none;">';
    foreach($this->options as $opt_name=>$opt_text) {
      echo '<li><input type="radio" name="'.$this->get_name().'" value="'.$opt_name.'" '.($this->get_value()==$opt_name ? 'checked="checked"' : '').' /> '.$opt_text;
    }
    echo '</ul>';
  }
}


class RangeField extends BaseField implements IField {
  private $min;
  private $max;
  private $step;
  private $min_text;
  private $max_text;
  private $tolerance; // only if values differ by more than this value it is considered as disagreement
  
  public function get_min() {
    return $this->min;
  }
  
  public function get_max() {
    return $this->max;
  }
  
  public function get_step() {
    return $this->step;
  }
  
  public function get_min_text() {
    return $this->min_text;
  }
  
  public function get_max_text() {
    return $this->max_text;
  }
  
  public function set_tolerance($value) {
    $this->tolerance = (float)$value;
  }
  
  public function get_tolerance() {
    return $this->tolerance;
  }
  
  function __construct($name, $min_text, $max_text, $min=0, $max=1, $step=0.1, $defaultvalue=0, $tolerance=0.2) {
    parent::__construct($name);
    $this->min = $min;
    $this->max = $max;
    $this->step = $step;
    $this->min_text = $min_text;
    $this->max_text = $max_text;
    $this->set_value($defaultvalue);
    $this->set_tolerance($tolerance);
  }
  
  public function db_value($value) {
    return (float)$value;
  }
  
  public function compare_values($value1, $value2) {
    if((float)$value1-(float)$value2<=$this->get_tolerance() and (float)$value2-(float)$value1<=$this->get_tolerance()) {
      return True;
    } else {
      return False;
    }
  }

  public function display() {
  ?>
  <div id="<?php echo $this->get_name(); ?>_field" class="range_field">
    <div style="text-align:center;">
    <span>min <?php echo $this->get_min(); ?></span>
    <input type="range" name="<?php echo $this->get_name(); ?>" value="<?php echo $this->get_value(); ?>" min="<?php echo $this->get_min(); ?>" max="<?php echo $this->get_max(); ?>" step="<?php echo $this->get_step(); ?>" onchange="<?php echo $this->get_name(); ?>_range.value=value" />
    <span>max <?php echo $this->get_max(); ?></span>
    </div>
    <span>("<?php echo $this->get_min_text(); ?>")</span><span style="float:right;">("<?php echo $this->get_max_text(); ?>")</span>
    <div style="text-align:center; margin-top:10px;"><span>current value: </span>
    <output id="<?php echo $this->get_name(); ?>_range" class="display:inline-block"><?php echo $this->get_value(); ?></output></div>
  </div>
  <?php
  }
}


class CheckListField extends RadioField implements IField {
  // $options given with constructor should each have (string_to_display_to_user, code, explanations)
  
  public function set_value($value) {
    $this->value = json_decode($value);
  }
  
  public function db_value($value) {
    return db_escape(json_encode($value),true);
  }
  
  public function compare_values($value1, $value2) {
    if(json_decode($value1, true)==json_decode($value2, true)) {
      return True;
    } else {
      return False;
    }
  }
  
  function __construct($name, $options) {
    parent::__construct($name, $options);
    $this->set_required(False);
  }
  
  public function display() {
    ?>
    <ul style="list-style:none">
    <?php foreach($this->get_options() as $option) { ?>
      <script type="text/javascript">
        function toggle_<?php echo $option[1]; ?>() {
          $("#<?php echo $this->get_name().'_'.$option[1]; ?>").toggle();
        };
      </script>
      <?php
        echo '<li><input type="checkbox" name="'.$this->get_name().'[]" value="'.$option[1].'" '.(in_array($option[1], $this->get_value())?'checked="checked"':'').' />';
          echo ' <span onclick="javascript:toggle_'.$option[1].'()" '.($option[2] ? 'class="help_hover"' : '').'><b>'.$option[0].'</b><div style="display:none" id="'.$this->get_name().'_'.$option[1].'">'.$option[2].'</div></span>';
        echo '</li>';
    } ?>
    </ul>
    <?php
  }
}


class BoundingBoxField extends BaseField implements IField {
  private $image_url;
  private $input_method;
  private $labels;
  static private $conflict_types = array(
    1=>'no bounding box for one of the annotations',
    2=>'different number of bounding boxes',
  );
  
  public function get_image_url() {
    return $this->image_url;
  }
  
  public function set_image_url($image_url) {
    $this->image_url = $image_url;
  }
  
  public function get_input_method() {
    return $this->input_method;
  }
  
  public function get_labels() {
    return $this->labels;
  }
  
  public function set_labels($labels) {
    $this->labels = $labels;
  }
  
  function __construct($name, $labels, $image_url='', $input_method='select') {
    // input method can be one of ['text', 'select', 'fixed']
    // labels should be array of strings
    parent::__construct($name);
    $this->image_url = $image_url;
    $this->labels = $labels;
    $this->input_method = $input_method;
    $this->set_required(False);
  }
  
  public function db_value($value) {
    return bb_sanitize($value,true);
  }

  public function display() { #TODO append index to var annotator in case there are several on one page
  ?>
    <div style="display:none;">
      <textarea id="<?php echo $this->get_name(); ?>_data" name="<?php echo $this->get_name(); ?>" rows="30" style="font-family:monospace;" readonly></textarea>
    </div>  
  
    <!-- Bounding box annotator -->
    <script type="text/javascript">
    $(document).ready(function() {
      // Initialize the bounding-box annotator.
      var annotator = new BBoxAnnotator({
        url: "<?php echo $this->get_image_url(); ?>",
        input_method: '<?php echo $this->get_input_method(); ?>',
        labels: [<?php
          $label_string = implode('","', $this->get_labels());
          echo '"'.$label_string.'"';
        ?>],
        onchange: function(entries) {
          $("#<?php echo $this->get_name(); ?>_data").text(JSON.stringify(entries, null, "  "));
        }
      });
      // Initialize the reset button.
      //$("#reset_button").click(function(e) {
      //  annotator.clear_all();
      //});
      // Load existing bounding box annotations (if editing response)
      <?php
      foreach(json_decode($this->get_value()) as $bbox) {
        echo 'new_rect(annotator, "'.$bbox->label .'", '.$bbox->left .', '.$bbox->top .', '.($bbox->left + $bbox->width - 1).', '.($bbox->top + $bbox->height - 1).'); ';
      }
      ?>
    });
    </script>
  <?php 
  }
  
  public function conflict_meaning($type_number) {
    // return a string with the meaning of the 
    if(array_key_exists($type_number, self::$conflict_types)) {
      return self::$conflict_types[$type_number];
    }
    return 'unknown conflict type';
  }
  
  public function compute_conflicts($annotation1, $annotation2) {
    // Compute array of concepts and associated types of disagreement in given annotations
    $disagreements = array();
    
    $bboxes1 = json_decode($annotation1, true);
    if(!$bboxes1) {
      $bboxes1 = array();
    }
    $bboxes2 = json_decode($annotation2, true);
    if(!$bboxes2) {
      $bboxes2 = array();
    }
    
    // for each concept check if bounding box annotations are different
    foreach($this->get_labels() as $concept) {   
      $concept_disagreements = array();
    
      // relevant annotations of first annotation
      $relevant_bboxes1 = array();
      foreach($bboxes1 as $bbox) {
        if($bbox['label']==strtolower($concept)) $relevant_bboxes1[] = $bbox;
      }
      
      // relevant annotations of second annotation
      $relevant_bboxes2 = array();
      foreach($bboxes2 as $bbox) {
        if($bbox['label']==strtolower($concept)) $relevant_bboxes2[] = $bbox;
      }
      
      if(count($relevant_bboxes1)!=count($relevant_bboxes2)) {
        if(count($relevant_bboxes1)*count($relevant_bboxes2)==0) {
          // Type 1: No bounding box for one of the annotations
          $concept_disagreements[] = 1;
        } else {
          // Type 2: Number of bounding boxes differs (but both annotators have annotated the concept)
          $concept_disagreements[] = 2;
        }
      }
      #TODO check for overlap
      if(count($concept_disagreements)) {
        $disagreements[$concept] = $concept_disagreements;
      }
    }
    return $disagreements;
  }
  
  public function compare_values($value1, $value2) {
    if(count($this->compute_conflicts($value1, $value2))>0) {
      return false;
    } else {
      return true;
    }
  }
}

?>
