<?php

// Basic task class
include_once('generic/BaseTask.class.php');

// Include field classes
include_once('generic/fields.php');

// Loading additional resources
include_once('generic/codes.php'); // bounding box labels


// Define the main class for the task
class ReconcileTask extends BaseTask {
  public $reconciled_task;

  function __construct($task_name, $annotator, $reconciled_task_name='main_task') {
    global $TASKS;
  
    parent::__construct($task_name, $annotator);
    
    // task-specific fields
    // (names given to Field constructors must correspond to database columns)
    $task_fields = array(
      'annotation1_id'=>new HiddenIntField('annotation1_id'),
      'annotation2_id'=>new HiddenIntField('annotation2_id'),
      'concept'=>new HiddenField('concept'),
      'decision'=>new HiddenIntField('decision'),
      'conflict_type'=>new HiddenField('conflict_type'),
      'bounding_boxes'=>new BoundingBoxField('bounding_boxes', bbox_concepts()),
      'reconciled_task'=>new HiddenField('reconciled_task', $reconciled_task_name),
    );
    foreach($task_fields as $field_name=>$field) {
      $this->set_field($field_name, $field);
    }
    
    // Load the task that is reconciled
    $task_class = $TASKS[$reconciled_task_name]['class_name'];
    include_once($task_class.'.class.php');
    $this->reconciled_task = new $task_class($reconciled_task_name, $this->annotator);
  }
  
  public function load_item($annotation_id=null, $item_id=null, $position_threshold=-1) {
    return $this->load_next_item();
  }
  
  public function load_next_item($position_threshold=-1) {
    $all_annotations = $this->reconciled_task->load_annotations($this->reconciled_task->settings()['users']); #TODO or all?    
    $annotations = array();
    foreach ($all_annotations as $ann) {
      if (!array_key_exists($ann['item_id'], $annotations)) {
        $annotations[$ann['item_id']] = array();
      }
      $annotations[$ann['item_id']][] = $ann;
    }
    
    // load reconciling votes
    $broken_ties = $this->load_annotations(array($this->annotator));
    $resolved_conflicts = array();
    foreach($broken_ties as $reconciling) {
      if(!array_key_exists($reconciling['item_id'], $resolved_conflicts)) {
        $resolved_conflicts[$reconciling['item_id']] = array();    
      }
      $resolved_conflicts[$reconciling['item_id']][] = $reconciling['concept'];
    }
    
    // find open conflicts
    foreach($annotations as $item_annotations) {
      if(count($item_annotations)<2) {
        continue;
      }

      $item_id = $item_annotations[0]['item_id'];
      
      $bbox_annotations = array();
      foreach($item_annotations as $annotation) {
        $bbox_annotations[] = $annotation['bounding_boxes'];
      }
      #TODO what if more than 2 annotations?
      foreach($this->reconciled_task->get_fields()['bounding_boxes']->compute_conflicts($bbox_annotations[0], $bbox_annotations[1])
              as $concept=>$conflict_types) {
        // skip conflicts that are already reconciled
        if(array_key_exists($item_id, $resolved_conflicts) and in_array($concept, $resolved_conflicts[$item_id])) {
          continue;
        }
        return array('item_id'=>$item_id, 'annotation1_id'=>$item_annotations[0]['id'], 'annotation2_id'=>$item_annotations[1]['id'], 'concept'=>$concept, 'conflict_type'=>$conflict_types[0]);
      }
    }
    
    // if there was a reconciling vote for all conflicts, see if there were any 'neither' cases
    foreach($broken_ties as $reconciling) {
      if(intval($reconciling['decision'])===0 and !$reconciling['bounding_boxes']) {
        return array('id'=>$reconciling['id'], 'item_id'=>$reconciling['item_id'], 'annotation1_id'=>$reconciling['annotation1_id'], 'annotation2_id'=>$reconciling['annotation2_id'], 'concept'=>$reconciling['concept'], 'conflict_type'=>$reconciling['conflict_type'], 'decision'=>0);
      }
    }
    
    return false;
  }
  
  public function get_first_annotation() {
    return $this->reconciled_task->load_annotation($this->get_fields()['annotation1_id']->get_value());
  }

  public function get_second_annotation() {
    return $this->reconciled_task->load_annotation($this->get_fields()['annotation2_id']->get_value());
  }
}

?>
