<?php

/***************************************************************************

    TASK CONFIGURATION

***************************************************************************/


// default settings (fields that are not specified for a task will use these values)
$default_values = array(
  'experts'=>array(), // users doing expert annotations
  'tie_breaker'=>array(), // users doing reconciling (for normal annotations only)
  'annotations_per_item'=>2,
  'expert_annotations_per_item'=>1,
  'data_table'=>'data',
  'user_table'=>'data_users',
);

// array with enabled tasks
// (if you want to globally disable a task, just comment it out here)
$TASKS = array(
  'main_task' => array(
                      'users'=>array('user', 'admin'), // users doing normal annotations
                      'experts'=>array('expert'), // users doing expert annotations
                      'tie_breaker'=>array('reconcile'), // users doing reconciling (for normal annotations only)
                      'header_name'=>'main_task',
                      'title'=>'Main Annotation Task',
                      'expert_annotations_per_item'=>2,
                      'class_name'=>'MainTask',
                      'page'=>'tweet_task', // which page to load for the task
                      'annotation_table'=>'annotations',
                      'field_reconcile_tasks'=>array('bounding_boxes'=>'reconcile_task'), // linking tasks that are used for reconciling special fields
                      ),
  'reconcile_task' => array(
                      'users'=>array('reconcile'), // users doing normal annotations
                      'header_name'=>'reconcile_task',
                      'title'=>'Bounding Box Reconcile Task',
                      'annotations_per_item'=>1,
                      'class_name'=>'ReconcileTask',
                      'page'=>'compare_task',
                      'annotation_table'=>'reconcile_annotations',
                      ),
  'bbox_task' => array(
                      'users'=>array('admin', 'admin'), // users doing normal annotations
                      'header_name'=>'bbox_task',
                      'title'=>'Bounding Box Annotation Task',
                      'class_name'=>'BBoxTask',
                      'page'=>'tweet_task', // which page to load for the task
                      'annotation_table'=>'bbox_annotations',
                      'field_reconcile_tasks'=>array('bounding_boxes'=>'reconcile_task'), // linking tasks that are used for reconciling special fields
                      ),
  'code_task' => array(
                      'users'=>array('user', 'admin'), // users doing normal annotations
                      'experts'=>array('expert'), // users doing expert annotations
                      'tie_breaker'=>array('reconcile'), // users doing reconciling (for normal annotations only)
                      'header_name'=>'code_task',
                      'title'=>'Code Annotation Task',
                      'expert_annotations_per_item'=>2,
                      'class_name'=>'CodeTask',
                      'page'=>'tweet_task', // which page to load for the task
                      'annotation_table'=>'code_annotations',
                      ),
  'collapsed_task' => array(
                      'users'=>array('admin'), // users doing normal annotations
                      'experts'=>array('expert'), // users doing expert annotations
                      'header_name'=>'collapsed_task',
                      'title'=>'Collapsed Code Annotation Task',
                      'expert_annotations_per_item'=>2,
                      'class_name'=>'CollapsedTask',
                      'page'=>'tweet_task', // which page to load for the task
                      'annotation_table'=>'collapsed_annotations',
                      ),
  );
  
// augmenting by default settings
foreach($TASKS as $task_name=>$task) {
  foreach($default_values as $field_name=>$default_value) {
    if(!array_key_exists($field_name, $task)) {
      $TASKS[$task_name][$field_name] = $default_value;
    }
  }
}

// collecting names of tasks that are used for reconciling special fields
$RECONCILE_TASKS = array();
foreach($TASKS as $task_name=>$task) {
  if(array_key_exists('field_reconcile_tasks', $task)) {
    foreach($task['field_reconcile_tasks'] as $field=>$reconcile_task) {
      $RECONCILE_TASKS[] = $reconcile_task;
    }
  }
}
?>
