<?php
// Security check to ensure that this file is loaded by the system.
if( !defined('MAIN_FILE_INCLUDED') || MAIN_FILE_INCLUDED!==true ) die('');

/***************************************************************************

    SYSTEM CONFIG

***************************************************************************/


/** SETTING THE TIME ZONE */

date_default_timezone_set('America/Chicago');


/** PATH AND URL CONSTANTS */

/** URL of the site */
define('BASE_URL', 'http://localhost/vatas/DEMO/');
    
/** URL of the image directory */
define('IMAGE_DIR_URL', 'http://localhost/vatas/DEMO_IMGS/');

/** Path to instructions folder */
define('INSTRUCTIONS_PATH', ABSPATH . 'tasks/instructions/');

/** Path to folder with functions */
define('FUNCTION_PATH', ABSPATH . 'functions/');


/** DATABASE */

// Which type of database to use
// Available options are mysql and psql.
// (There has to be a corresponding file database.DB_TYPE.php in the function folder.)
define('DB_TYPE', 'psql');

// Host of the database
define('DB_HOST', 'localhost' );

// Name of the user
define('DB_USER', 'db_user' );

// Password for this user
define('DB_PASSWORD', 'passwordforthedb' );

// Name of the database
define('DB_DB', 'vatas' );


/** SYSTEM PAGES */

$PAGES = array(
    'login'=>array('name'=>'login', 'title'=>'Login'),
    'home'=>array('name'=>'home', 'title'=>'Welcome to VATAS!'),
    'annotations'=>array('name'=>'annotations', 'title'=>'Annotation Overview'),
    'export'=>array('name'=>'export', 'title'=>'Export Annotations'),
    );


/** USERS */

$USERS = array(
    'admin'=>array('name'=>'admin', 'is_admin'=>'t', 'password'=>'21232f297a57a5a743894a0e4a801fc3'),
    'user'=>array('name'=>'user', 'is_admin'=>'f', 'password'=>'ee11cbb19052e40b07aac0ca060c23ee'),
    'expert'=>array('name'=>'expert', 'is_admin'=>'f', 'password'=>'b9b83bad6bd2b4f7c40109304cf580e1'),
    'reconcile'=>array('name'=>'reconcile', 'is_admin'=>'t', 'password'=>'378d41584078efc0587e7dfc62b2ae8b'),
    );


/** OTHER SETTINGS */

// Used for deciding which error messages and warnings to display to the user
define("DEBUG_MODE", false);
?>
